﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using Microsoft.Reporting.WinForms;


namespace MidProject2.Forms
{
    public partial class AttendanceList : Form
    {

        SqlDataAdapter da;

        DataTable dt;

        public AttendanceList()
        {
            InitializeComponent();
        }

        private void AttendanceList_Load(object sender, EventArgs e)
        {
            var n = Configuration.getInstance().getConnection();
            da = new SqlDataAdapter("Select StudentAttendance.StudentId , Student.RegistrationNumber, CONCAT(Student.FirstName,Student.LastName) AS FullName , Lookup.Name AS Attendance ,ClassAttendance.AttendanceDate from Student LEFT JOIN StudentAttendance ON Student.Id = StudentAttendance.StudentId INNER JOIN Lookup ON Lookup.LookupId = StudentAttendance.AttendanceStatus INNER JOIN ClassAttendance ON ClassAttendance.Id = StudentAttendance.AttendanceId", n);
            dt = new DataTable();
            da.Fill(dt);
            reportViewer1.LocalReport.DataSources.Clear();
            ReportDataSource rds = new ReportDataSource("DataSet2", dt);
            reportViewer1.LocalReport.ReportPath = @"C:\Users\BestBoi\Desktop\LATEST\MidProject2\MidProject2\Reports\Attendance.rdlc";

            reportViewer1.LocalReport.DataSources.Add(rds);
            this.reportViewer1.RefreshReport();
        }
    }
}
