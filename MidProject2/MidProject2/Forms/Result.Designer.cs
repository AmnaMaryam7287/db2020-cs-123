﻿namespace MidProject2.Forms
{
    partial class Result
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Fullgrid = new System.Windows.Forms.DataGridView();
            this.maxlvltxt = new System.Windows.Forms.TextBox();
            this.C = new System.Windows.Forms.Label();
            this.Printbtn = new System.Windows.Forms.Button();
            this.Stuidcbx = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.FullNametxt = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.Fullgrid)).BeginInit();
            this.SuspendLayout();
            // 
            // Fullgrid
            // 
            this.Fullgrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.Fullgrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Fullgrid.Location = new System.Drawing.Point(12, 80);
            this.Fullgrid.Name = "Fullgrid";
            this.Fullgrid.Size = new System.Drawing.Size(801, 243);
            this.Fullgrid.TabIndex = 89;
            // 
            // maxlvltxt
            // 
            this.maxlvltxt.Location = new System.Drawing.Point(40, 160);
            this.maxlvltxt.Name = "maxlvltxt";
            this.maxlvltxt.Size = new System.Drawing.Size(100, 20);
            this.maxlvltxt.TabIndex = 90;
            // 
            // C
            // 
            this.C.AutoSize = true;
            this.C.Font = new System.Drawing.Font("Arial", 19F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.C.ForeColor = System.Drawing.Color.White;
            this.C.Location = new System.Drawing.Point(286, 9);
            this.C.Name = "C";
            this.C.Size = new System.Drawing.Size(182, 31);
            this.C.TabIndex = 91;
            this.C.Text = "Student Result";
            // 
            // Printbtn
            // 
            this.Printbtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Printbtn.Font = new System.Drawing.Font("Century Gothic", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Printbtn.ForeColor = System.Drawing.Color.White;
            this.Printbtn.Location = new System.Drawing.Point(318, 329);
            this.Printbtn.Name = "Printbtn";
            this.Printbtn.Size = new System.Drawing.Size(133, 54);
            this.Printbtn.TabIndex = 92;
            this.Printbtn.Text = "Report";
            this.Printbtn.UseVisualStyleBackColor = true;
            this.Printbtn.Click += new System.EventHandler(this.Printbtn_Click);
            // 
            // Stuidcbx
            // 
            this.Stuidcbx.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.Stuidcbx.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Stuidcbx.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Stuidcbx.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Stuidcbx.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Stuidcbx.FormattingEnabled = true;
            this.Stuidcbx.Location = new System.Drawing.Point(105, 46);
            this.Stuidcbx.Name = "Stuidcbx";
            this.Stuidcbx.Size = new System.Drawing.Size(192, 28);
            this.Stuidcbx.TabIndex = 95;
            this.Stuidcbx.SelectedIndexChanged += new System.EventHandler(this.Stuidcbx_SelectedIndexChanged);
            this.Stuidcbx.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Stuidcbx_KeyDown);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(12, 54);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 20);
            this.label1.TabIndex = 96;
            this.label1.Text = "Student Id:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(303, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(116, 20);
            this.label2.TabIndex = 94;
            this.label2.Text = "Student Name:";
            // 
            // FullNametxt
            // 
            this.FullNametxt.Enabled = false;
            this.FullNametxt.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FullNametxt.Location = new System.Drawing.Point(425, 49);
            this.FullNametxt.Name = "FullNametxt";
            this.FullNametxt.Size = new System.Drawing.Size(388, 25);
            this.FullNametxt.TabIndex = 93;
            // 
            // Result
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ClientSize = new System.Drawing.Size(825, 394);
            this.Controls.Add(this.Stuidcbx);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.FullNametxt);
            this.Controls.Add(this.Printbtn);
            this.Controls.Add(this.C);
            this.Controls.Add(this.Fullgrid);
            this.Controls.Add(this.maxlvltxt);
            this.MaximizeBox = false;
            this.Name = "Result";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Result";
            this.Load += new System.EventHandler(this.Result_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Fullgrid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView Fullgrid;
        private System.Windows.Forms.TextBox maxlvltxt;
        private System.Windows.Forms.Label C;
        private System.Windows.Forms.Button Printbtn;
        private System.Windows.Forms.ComboBox Stuidcbx;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox FullNametxt;
    }
}