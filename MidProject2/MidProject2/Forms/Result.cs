﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace MidProject2.Forms
{
    public partial class Result : Form
    {
        SqlCommand cmd;
        SqlDataAdapter da;
        SqlDataReader dr;
        DataTable dt;
        DataSet ds;

        public Result()
        {
            InitializeComponent();
        }

        private void Result_Load(object sender, EventArgs e)
        {
            var cc = Configuration.getInstance().getConnection();
          
            dt = new DataTable();
            //con5.Open();
            dr = null;
            cmd = new SqlCommand("SELECT  MAX(MeasurementLevel) AS MaxLvl FROM RubricLevel ", cc);
            dr = cmd.ExecuteReader();

            while (dr.Read())
            {
                maxlvltxt.Text = dr["MaxLvl"].ToString();


            }
            dr.Close();

            var con = Configuration.getInstance().getConnection();
            da = new SqlDataAdapter("SELECT * FROM Student ", con);

            dt = new DataTable();
            da.Fill(dt);
            foreach (DataRow ROW in dt.Rows)
            {
                Stuidcbx.Items.Add(ROW["Id"].ToString());

            }


            ShowData();
        }


        public void ShowData()
        {
            //cmd = new SqlCommand("Select Student.Id AS Stud_Id ,CONCAT(Student.FirstName,Student.LastName) AS Stud_Name , AssessmentComponent.TotalMarks , AssessmentComponent.Name , Rubric.Details, RubricLevel.MeasurementLevel AS RubricLevel,RubricLevel.Details AS LevelDesc FROM Student INNER JOIN StudentResult ON Student.Id = StudentResult.StudentId  INNER JOIN AssessmentComponent INNER JOIN Rubric ON AssessmentComponent.RubricId = Rubric.Id ON StudentResult.AssessmentComponentId = AssessmentComponent.Id INNER JOIN RubricLevel ON  StudentResult.RubricMeasurementId = RubricLevel.Id", oo);
            
            var oo = Configuration.getInstance().getConnection();
            cmd = new SqlCommand("Select Student.Id AS Stud_Id, CONCAT(Student.FirstName,Student.LastName) AS Stud_Name , AssessmentComponent.TotalMarks AS Comp_Marks , AssessmentComponent.Name AS Component , Rubric.Details AS Rubric, RubricLevel.MeasurementLevel AS Stu_RubricLevel,RubricLevel.Details AS LevelDesc , ((CONVERT(FLOAT, RubricLevel.MeasurementLevel)/'" + maxlvltxt.Text + "')*  CONVERT(FLOAT, AssessmentComponent.TotalMarks)) AS ObtainedMarks , Clo.Name AS CLO ,Assessment.Title AS Assessment FROM Student INNER JOIN StudentResult ON Student.Id = StudentResult.StudentId  INNER JOIN AssessmentComponent ON StudentResult.AssessmentComponentId = AssessmentComponent.Id INNER JOIN Rubric ON AssessmentComponent.RubricId = Rubric.Id INNER JOIN Clo ON Clo.Id = Rubric.CloId INNER JOIN Assessment ON Assessment.Id = AssessmentComponent.AssessmentId  INNER JOIN RubricLevel ON  StudentResult.RubricMeasurementId = RubricLevel.Id", oo);
            da = new SqlDataAdapter(cmd);
            dt = new DataTable();
            da.Fill(dt);
            Fullgrid.DataSource = dt;
        }

        private void Printbtn_Click(object sender, EventArgs e)
        {
            var ResultList = new Forms.ResultList();
            ResultList.Show();
        }

        private void Stuidcbx_SelectedIndexChanged(object sender, EventArgs e)
        {
            var cc = Configuration.getInstance().getConnection();
            dt = new DataTable();
            //con5.Open();
            dr = null;
            cmd = new SqlCommand("SELECT  CONCAT(FirstName,LastName) AS FullName FROM Student WHERE Id = '" + Stuidcbx.Text + "'", cc);
            dr = cmd.ExecuteReader();

            while (dr.Read())
            {
                FullNametxt.Text = dr["FullName"].ToString();


            }
            dr.Close();


            var oo = Configuration.getInstance().getConnection();
            if (Stuidcbx.Text.Trim().Length == 0)
            {
                cmd = new SqlCommand("Select Student.Id AS Stud_Id, CONCAT(Student.FirstName,Student.LastName) AS Stud_Name , AssessmentComponent.TotalMarks AS Comp_Marks , AssessmentComponent.Name AS Component , Rubric.Details AS Rubric, RubricLevel.MeasurementLevel AS Stu_RubricLevel,RubricLevel.Details AS LevelDesc , ((CONVERT(FLOAT, RubricLevel.MeasurementLevel)/'" + maxlvltxt.Text + "')*  CONVERT(FLOAT, AssessmentComponent.TotalMarks)) AS ObtainedMarks , Clo.Name AS CLO ,Assessment.Title AS Assessment FROM Student INNER JOIN StudentResult ON Student.Id = StudentResult.StudentId  INNER JOIN AssessmentComponent ON StudentResult.AssessmentComponentId = AssessmentComponent.Id INNER JOIN Rubric ON AssessmentComponent.RubricId = Rubric.Id INNER JOIN Clo ON Clo.Id = Rubric.CloId INNER JOIN Assessment ON Assessment.Id = AssessmentComponent.AssessmentId  INNER JOIN RubricLevel ON  StudentResult.RubricMeasurementId = RubricLevel.Id  ", oo);
            }
            else
            {
                cmd = new SqlCommand("Select Student.Id AS Stud_Id, CONCAT(Student.FirstName,Student.LastName) AS Stud_Name , AssessmentComponent.TotalMarks AS Comp_Marks , AssessmentComponent.Name AS Component , Rubric.Details AS Rubric, RubricLevel.MeasurementLevel AS Stu_RubricLevel,RubricLevel.Details AS LevelDesc , ((CONVERT(FLOAT, RubricLevel.MeasurementLevel)/'" + maxlvltxt.Text + "')*  CONVERT(FLOAT, AssessmentComponent.TotalMarks)) AS ObtainedMarks , Clo.Name AS CLO ,Assessment.Title AS Assessment FROM Student INNER JOIN StudentResult ON Student.Id = StudentResult.StudentId  INNER JOIN AssessmentComponent ON StudentResult.AssessmentComponentId = AssessmentComponent.Id INNER JOIN Rubric ON AssessmentComponent.RubricId = Rubric.Id INNER JOIN Clo ON Clo.Id = Rubric.CloId INNER JOIN Assessment ON Assessment.Id = AssessmentComponent.AssessmentId  INNER JOIN RubricLevel ON  StudentResult.RubricMeasurementId = RubricLevel.Id  WHERE Student.Id = '" + Stuidcbx.Text + "' ", oo);
       
            }
            da = new SqlDataAdapter(cmd);
            dt = new DataTable();
            da.Fill(dt);
            Fullgrid.DataSource = dt;
        }

        private void Stuidcbx_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                Stuidcbx.Text = null;
            }
        }
    }
}
