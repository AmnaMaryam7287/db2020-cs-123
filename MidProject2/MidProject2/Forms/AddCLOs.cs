﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace MidProject2.Forms
{
    public partial class AddCLOs : Form
    {

        SqlCommand cmd;
        SqlDataAdapter da;
        SqlDataReader dr;
        DataTable dt;
        DataSet ds;

        public AddCLOs()
        {
            InitializeComponent();
        }

        private void AddCLOs_Load(object sender, EventArgs e)
        {
            ShowData();
        }

        public void ShowData()
        {

            var oo = Configuration.getInstance().getConnection();
            cmd = new SqlCommand("Select * FROM Clo", oo);
            da = new SqlDataAdapter(cmd);
            dt = new DataTable();
            da.Fill(dt);
            Fullgrid.DataSource = dt;
        }

         public void Clear()
        {
            foreach (var item in this.Controls)
            {
                if (item.GetType().Equals(typeof(TextBox)))
                {
                    TextBox t1 = item as TextBox;
                    t1.Text = string.Empty;
                }

            }
            }

        private void Savebtn_Click(object sender, EventArgs e)
        {

            if (Clotxt.Text.Trim().Length == 0)
            {
                MessageBox.Show("Fields Should Not Be Empty!", "User Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);

            }
            else
            {

                var con = Configuration.getInstance().getConnection();
                cmd = new SqlCommand("INSERT INTO Clo (Name, DateCreated,DateUpdated) VALUES (@Name, @DateCreated,@DateUpdated)", con);
                cmd.Parameters.AddWithValue("@Name", Clotxt.Text);
                cmd.Parameters.AddWithValue("@DateCreated", DateTime.Now.ToShortDateString());
                cmd.Parameters.AddWithValue("@DateUpdated", DateTime.Now.ToShortDateString());
                cmd.ExecuteNonQuery();
                MessageBox.Show("Successfully Saved", "User Information", MessageBoxButtons.OK, MessageBoxIcon.Information);

                ShowData();
                Clear();
            }
        }

        private void AddCLOs_Activated(object sender, EventArgs e)
        {
            Clotxt.Focus();
        }

        private void Updatebtn_Click(object sender, EventArgs e)
        {
            if (Clotxt .Text.Trim().Length == 0 )
            {
                MessageBox.Show("Fields Should Not Be Empty!", "User Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);

            }
            else
            {



                var nn = Configuration.getInstance().getConnection();
                string query = "UPDATE Clo SET Name = '" + Clotxt.Text + "' , DateUpdated = '" + DateTime.Now.ToShortDateString() + "'  WHERE Id = '" + Cloidtxt.Text + "'";
                cmd = new SqlCommand(query, nn);
                //SqlDataReader dr;
                try
                {
                    //con.Open();
                    dr = cmd.ExecuteReader();
                    MessageBox.Show("Record Updated successfully", "User Information", MessageBoxButtons.OK, MessageBoxIcon.Information);


                    while (dr.Read())
                    {
                    }
                    //con.Close();
                    dr.Close();
                }
                catch (Exception ec)
                {
                    MessageBox.Show(ec.Message);
                }

                ShowData();

                Clear();
            }
        }

        private void Fullgrid_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                //gets a collection that contains all the rows
                DataGridViewRow row = this.Fullgrid.Rows[e.RowIndex];
                Cloidtxt.Text = row.Cells[0].Value.ToString();
               
                Clotxt.Text = row.Cells[1].Value.ToString();
                  }
        }

        private void Deletebtn_Click(object sender, EventArgs e)
        {

            if (Clotxt .Text.Trim().Length == 0 )
            {
                MessageBox.Show("Fields Should Not Be Empty!", "User Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);

            }
            else
            {

             DialogResult result = MessageBox.Show("YOU SURE WANT TO DELETE IT?", "QUESTION", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
             if (result == DialogResult.Yes)
             {
                 var o = Configuration.getInstance().getConnection();
                 cmd = new SqlCommand("DELETE  from Clo WHERE Id = '" + Cloidtxt.Text + "'", o);
                 cmd.ExecuteNonQuery();
                 MessageBox.Show("Record Deleted successfully", "User Information", MessageBoxButtons.OK, MessageBoxIcon.Information);

                 ShowData();

                 Clear();
             }
             }
        }
    }
}
