﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;


namespace MidProject2.Forms
{
    public partial class AddRubricsLevel : Form
    {

        SqlCommand cmd;
        SqlDataAdapter da;
        SqlDataReader dr;
        DataTable dt;
        DataSet ds;

        public AddRubricsLevel()
        {
            InitializeComponent();
        }


        public void Clear()
        {
            foreach (var item in this.Controls)
            {
                if (item.GetType().Equals(typeof(TextBox)))
                {
                    TextBox t1 = item as TextBox;
                    t1.Text = string.Empty;
                }

                if (item.GetType().Equals(typeof(ComboBox)))
                {
                    ComboBox c1 = item as ComboBox;
                    c1.Text = null;
                }

            }
        }


        public void ShowData()
        {

            var oo = Configuration.getInstance().getConnection();
            cmd = new SqlCommand("Select Rubric.Id AS RubricId, Rubric.Details AS RubricName, RubricLevel.MeasurementLevel AS RubricLevel,RubricLevel.Details AS LevelDescription , RubricLevel.Id AS RubricLevelId  from Rubric LEFT JOIN RubricLevel ON Rubric.Id = RubricLevel.RubricId ", oo);
            da = new SqlDataAdapter(cmd);
            dt = new DataTable();
            da.Fill(dt);
            Fullgrid.DataSource = dt;
        }

        private void AddRubricsLevel_Load(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            da = new SqlDataAdapter("SELECT * FROM Rubric", con);
            dt = new DataTable();
            da.Fill(dt);
            foreach (DataRow ROW in dt.Rows)
            {
                Ridcbx.Items.Add(ROW["Id"].ToString());
                Searchcbx.Items.Add(ROW["Id"].ToString());

            }

            ShowData();
            Fullgrid.BringToFront();

         

                Fullgrid.Columns[0].ReadOnly = true;
                Fullgrid.Columns[1].ReadOnly = true;
                
            
        }

        private void Ridcbx_SelectedIndexChanged(object sender, EventArgs e)
        {
            var cc = Configuration.getInstance().getConnection();
            dt = new DataTable();
            //con5.Open();
            dr = null;
            cmd = new SqlCommand("SELECT * FROM Rubric WHERE Id = '" + Ridcbx.Text + "'", cc);
            dr = cmd.ExecuteReader();

            while (dr.Read())
            {
                Rnametxt.Text = dr["Details"].ToString();


            }
            dr.Close();
        }

        private void Addlvlbtn_Click(object sender, EventArgs e)
        {

            if (lvltxt.Text.Trim().Length == 0 || lvldesctxt.Text.Trim().Length == 0)
            {
                MessageBox.Show("Fields Should Not Be Empty!", "User Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);

            }
            else
            {
                int row = 0;
                Addgrid.Rows.Add();
                row = Addgrid.Rows.Count - 2;
                Addgrid["MeasurementLevel", row].Value = lvltxt.Text;
                Addgrid["Details", row].Value = lvldesctxt.Text;

                lvltxt.Clear();
                lvldesctxt.Clear();
                lvltxt.Focus();

                Addgrid.BringToFront();
            }

           

        }

        private void Savebtn_Click(object sender, EventArgs e)
        {

             if (Ridcbx.Text.Trim().Length == 0 || Rnametxt.Text.Trim().Length == 0 )
            {
                MessageBox.Show("Fields Should Not Be Empty!", "User Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);

            }

            else
            {

                for (int i = 0; i < Addgrid.Rows.Count - 1; i++)
                {

                    var con = Configuration.getInstance().getConnection();
                    cmd = new SqlCommand("INSERT INTO RubricLevel (RubricId, Details,MeasurementLevel) VALUES (@RubricId, '" + Addgrid.Rows[i].Cells[1].Value + "' , '" + Addgrid.Rows[i].Cells[0].Value + "')", con);
                    cmd.Parameters.AddWithValue("@RubricId", Ridcbx.Text);
                    cmd.ExecuteNonQuery();
                   
                    

                }

                MessageBox.Show("Successfully saved", "User Information", MessageBoxButtons.OK, MessageBoxIcon.Information);


                ShowData();
                Clear();
                Ridcbx.Focus();
                Addgrid.Rows.Clear();
//                Addgrid.DataSource = null;
                Fullgrid.BringToFront();


            }
        }

        private void AddRubricsLevel_Activated(object sender, EventArgs e)
        {
            Ridcbx.Focus();
        }

        private void Updatebtn_Click(object sender, EventArgs e)
        {
            if (Searchcbx.Text.Trim().Length == 0 )
            {
                MessageBox.Show("Fields Should Not Be Empty!", "User Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);

            }

            else
            {



                for (int i = 0; i < Fullgrid.Rows.Count - 1; i++)
                {
                    

                    var nn = Configuration.getInstance().getConnection();
                    string query;
                    if (Rlvlidtxt.Text == "")
                    {
                         query = "UPDATE RubricLevel SET  Details = '" + Fullgrid.Rows[i].Cells[3].Value + "'  , MeasurementLevel = '" + Fullgrid.Rows[i].Cells[2].Value + "' WHERE RubricId = '" + Searchcbx.Text + "' AND Id = '" + Fullgrid.Rows[i].Cells[4].Value + "' ";
                    
                    }
                    else
                    {
                        query = "UPDATE RubricLevel SET  Details = '" + lvldesctxt.Text + "'  , MeasurementLevel = '" + lvltxt.Text + "' WHERE RubricId = '" + Searchcbx.Text + "' AND Id = '" + Rlvlidtxt.Text + "' ";

                    }
                    cmd = new SqlCommand(query, nn);
                    //SqlDataReader dr;
                    try
                    {
                        //con.Open();
                        dr = cmd.ExecuteReader();
                      

                        while (dr.Read())
                        {
                        }
                        //con.Close();
                        dr.Close();
                    }
                    catch (Exception ec)
                    {
                        MessageBox.Show(ec.Message);
                    }

                    
                }

                MessageBox.Show("Record Updated successfully", "User Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                ShowData();

                Clear();
            }
        }

        private void Searchcbx_SelectedIndexChanged(object sender, EventArgs e)
        {
            Fullgrid.BringToFront();

         

                var oo = Configuration.getInstance().getConnection();
                if (Searchcbx.Text.Trim().Length == 0)
                {
                    cmd = new SqlCommand("Select Rubric.Id AS RubricId, Rubric.Details AS RubricName, RubricLevel.MeasurementLevel AS RubricLevel ,RubricLevel.Details AS LevelDescription , RubricLevel.Id AS RubricLevelId  from Rubric LEFT JOIN RubricLevel ON Rubric.Id = RubricLevel.RubricId ", oo);
                
                }
                else
                {
                    cmd = new SqlCommand("Select Rubric.Id AS RubricId, Rubric.Details AS RubricName, RubricLevel.MeasurementLevel AS RubricLevel ,RubricLevel.Details AS LevelDescription , RubricLevel.Id AS RubricLevelId  from Rubric LEFT JOIN RubricLevel ON Rubric.Id = RubricLevel.RubricId WHERE Rubric.Id ='" + Searchcbx.Text + "'  ", oo);
               
                }
                da = new SqlDataAdapter(cmd);
                dt = new DataTable();
                da.Fill(dt);
                Fullgrid.DataSource = dt;
                Fullgrid.Columns[4].ReadOnly = true;
            
        }

        private void Deletebtn_Click(object sender, EventArgs e)
        {
            if (Searchcbx.Text.Trim().Length == 0)
            {
                MessageBox.Show("Fields Should Not Be Empty!", "User Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);

            }

            else
            {

             DialogResult result = MessageBox.Show("YOU SURE WANT TO DELETE IT?", "QUESTION", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                 if (result == DialogResult.Yes)
                 {

                     for (int i = 0; i < Fullgrid.Rows.Count - 1; i++)
                     {


                         var nn = Configuration.getInstance().getConnection();
                         string query;
                         if (Rlvlidtxt.Text == "")
                         {
                              query = "DELETE  from RubricLevel WHERE RubricId = '" + Searchcbx.Text + "' AND Id = '" + Fullgrid.Rows[i].Cells[4].Value + "' ";
                       
                         }
                         else
                         {
                              query = "DELETE  from RubricLevel WHERE RubricId = '" + Searchcbx.Text + "' AND Id = '" + Rlvlidtxt.Text + "' ";
                       
                         }
                         cmd = new SqlCommand(query, nn);
                         //SqlDataReader dr;
                         try
                         {
                             //con.Open();
                             dr = cmd.ExecuteReader();


                             while (dr.Read())
                             {
                             }
                             //con.Close();
                             dr.Close();
                         }
                         catch (Exception ec)
                         {
                             MessageBox.Show(ec.Message);
                         }

                     }
                }

                 MessageBox.Show("Record Deleted successfully", "User Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                 ShowData();

                Clear();
            }
        }

        private void Fullgrid_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {

                if (Searchcbx.Text != "")
                {


                    //gets a collection that contains all the rows
                    DataGridViewRow row = this.Fullgrid.Rows[e.RowIndex];
                    Searchcbx.Text = row.Cells[0].Value.ToString();
                    lvltxt.Text = row.Cells[2].Value.ToString();
                    lvldesctxt.Text = row.Cells[3].Value.ToString();
                    Rlvlidtxt.Text = row.Cells[4].Value.ToString();
                }
            }
        }

        private void Searchcbx_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Escape)
            {
                Searchcbx.Text = null;
            }
        }

        private void lvltxt_TextChanged(object sender, EventArgs e)
        {
            var cc = Configuration.getInstance().getConnection();
            dt = new DataTable();
            //con5.Open();
            dr = null;
            cmd = new SqlCommand("SELECT * FROM RubricLevel WHERE RubricId = '" + Searchcbx.Text + "' AND MeasurementLevel = '" + lvltxt.Text + "'", cc);
            dr = cmd.ExecuteReader();

            while (dr.Read())
            {
                Rlvlidtxt.Text = dr["Id"].ToString();


            }
            dr.Close();
        }
    }
}
