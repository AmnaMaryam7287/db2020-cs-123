﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace MidProject2.Forms
{
    public partial class StudentEvaluation : Form
    {

        SqlCommand cmd;
        SqlDataAdapter da;
        SqlDataReader dr;
        DataTable dt;
        DataSet ds;
        public StudentEvaluation()
        {
            InitializeComponent();
        }

        private void StudentEvaluation_Load(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            da = new SqlDataAdapter("SELECT * FROM Student ", con);
           
            dt = new DataTable();
            da.Fill(dt);
            foreach (DataRow ROW in dt.Rows)
            {
                Stuidcbx.Items.Add(ROW["Id"].ToString());

            }


            da = new SqlDataAdapter("SELECT * FROM AssessmentComponent", con);
            dt = new DataTable();
            da.Fill(dt);
            foreach (DataRow ROW in dt.Rows)
            {
                Assidcbx.Items.Add(ROW["Id"].ToString());
     
            }

            da = new SqlDataAdapter("SELECT * FROM RubricLevel", con);
            dt = new DataTable();
            da.Fill(dt);
            foreach (DataRow ROW in dt.Rows)
            {
                lvlidcbx.Items.Add(ROW["Id"].ToString());
                
            }

            ShowData();

        }

        public void Clear()
        {
            foreach (var item in this.Controls)
            {
                if (item.GetType().Equals(typeof(TextBox)))
                {
                    TextBox t1 = item as TextBox;
                    t1.Text = string.Empty;
                }

                if (item.GetType().Equals(typeof(ComboBox)))
                {
                    ComboBox c1 = item as ComboBox;
                    c1.Text = null;
                }

            }
        }


        public void ShowData()
        {

            var oo = Configuration.getInstance().getConnection();
            cmd = new SqlCommand("Select Student.Id AS Stud_Id ,CONCAT(Student.FirstName,Student.LastName) AS Stud_Name , AssessmentComponent.Id AS Ass_Comp_Id , AssessmentComponent.Name , RubricLevel.MeasurementLevel AS RubricLevel,RubricLevel.Details AS LevelDesc , StudentResult.EvaluationDate  FROM Student INNER JOIN StudentResult ON Student.Id = StudentResult.StudentId  INNER JOIN AssessmentComponent ON StudentResult.AssessmentComponentId = AssessmentComponent.Id INNER JOIN RubricLevel ON  StudentResult.RubricMeasurementId = RubricLevel.Id ", oo);
            da = new SqlDataAdapter(cmd);
            dt = new DataTable();
            da.Fill(dt);
            Fullgrid.DataSource = dt;
        }

        private void Stuidcbx_SelectedIndexChanged(object sender, EventArgs e)
        {
            var cc = Configuration.getInstance().getConnection();
            dt = new DataTable();
            //con5.Open();
            dr = null;
            cmd = new SqlCommand("SELECT  CONCAT(FirstName,LastName) AS FullName FROM Student WHERE Id = '" + Stuidcbx.Text + "'", cc);
            dr = cmd.ExecuteReader();

            while (dr.Read())
            {
                FullNametxt.Text = dr["FullName"].ToString();


            }
            dr.Close();
        }

        private void Assidcbx_SelectedIndexChanged(object sender, EventArgs e)
        {
            var cc = Configuration.getInstance().getConnection();
            dt = new DataTable();
            //con5.Open();
            dr = null;
            cmd = new SqlCommand("SELECT * FROM AssessmentComponent WHERE Id = '" + Assidcbx.Text + "'", cc);
            dr = cmd.ExecuteReader();

            while (dr.Read())
            {
                Titletxt.Text = dr["Name"].ToString();


            }
            dr.Close();
        }

        private void Savebtn_Click(object sender, EventArgs e)
        {

            if (Stuidcbx.Text.Trim().Length == 0 || Assidcbx.Text.Trim().Length == 0 || lvlidcbx.Text.Trim().Length == 0)
            {
                MessageBox.Show("Fields Should Not Be Empty!", "User Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);

            }
            else
            {

                var con = Configuration.getInstance().getConnection();
                cmd = new SqlCommand("INSERT INTO StudentResult (StudentId,AssessmentComponentId, RubricMeasurementId,EvaluationDate) VALUES (@StudentId,@AssessmentComponentId, @RubricMeasurementId,@EvaluationDate)", con);
                cmd.Parameters.AddWithValue("@StudentId", Stuidcbx.Text);
                cmd.Parameters.AddWithValue("@AssessmentComponentId", Assidcbx.Text);
                cmd.Parameters.AddWithValue("@RubricMeasurementId", lvlidcbx.Text);
                cmd.Parameters.AddWithValue("@EvaluationDate", DateTime.Now.ToShortDateString());

                cmd.ExecuteNonQuery();
                MessageBox.Show("Successfully saved", "User Information", MessageBoxButtons.OK, MessageBoxIcon.Information);

                ShowData();
                Clear();
                Assidcbx.Focus();
            }
             }

        private void lvlidcbx_SelectedIndexChanged(object sender, EventArgs e)
        {
            var cc = Configuration.getInstance().getConnection();
           DataTable dt1 = new DataTable();
            //con5.Open();
            SqlDataReader dr1 = null;
            SqlCommand cmd1 = new SqlCommand("SELECT * FROM RubricLevel WHERE Id = '" + lvlidcbx.Text + "'", cc);
            dr1 = cmd1.ExecuteReader();

            while (dr1.Read())
            {
                lvldesctxt.Text = dr1["Details"].ToString();
                lvltxt.Text = dr1["MeasurementLevel"].ToString();


            }
            dr1.Close();
        }

        private void Updatebtn_Click(object sender, EventArgs e)
        {
            //var kk = Configuration.getInstance().getConnection();
            //dt = new DataTable();
            ////con5.Open();
            //dr = null;
            //cmd = new SqlCommand("SELECT * FROM RubricLevel WHERE MeasurementLevel = '" + lvltxt.Text + "' AND Details = '" + lvldesctxt.Text + "'", kk);
            //dr = cmd.ExecuteReader();

            //while (dr.Read())
            //{
            //    lvlidcbx.Text = dr["Id"].ToString();

            if (Stuidcbx.Text.Trim().Length == 0 || Assidcbx.Text.Trim().Length == 0 || lvlidcbx.Text.Trim().Length == 0)
            {
                MessageBox.Show("Fields Should Not Be Empty!", "User Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);

            }
            else
            {



                var nn = Configuration.getInstance().getConnection();
                string query = "UPDATE StudentResult SET RubricMeasurementId = '" + lvlidcbx.Text + "' WHERE StudentId = '" + Stuidcbx.Text + "' AND AssessmentComponentId = '" + Assidcbx.Text + "' ";
                cmd = new SqlCommand(query, nn);
                //SqlDataReader dr;
                try
                {
                    //con.Open();
                    dr = cmd.ExecuteReader();
                    MessageBox.Show("Record Updated successfully", "User Information", MessageBoxButtons.OK, MessageBoxIcon.Information);


                    while (dr.Read())
                    {
                    }
                    //con.Close();
                    dr.Close();
                }
                catch (Exception ec)
                {
                    MessageBox.Show(ec.Message);
                }

                ShowData();

                Clear();
            }



            //}
            //dr.Close();



        }

        private void Fullgrid_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                //gets a collection that contains all the rows
                DataGridViewRow row = this.Fullgrid.Rows[e.RowIndex];
                Stuidcbx.Text = row.Cells[0].Value.ToString();
                //FullNametxt.Text = row.Cells[1].Value.ToString();
                Assidcbx.Text = row.Cells[2].Value.ToString();
               // Titletxt.Text = row.Cells[3].Value.ToString();
                //lvlidcbx.Text = row.Cells[4].Value.ToString();
                lvltxt.Text = row.Cells[4].Value.ToString();
                lvldesctxt.Text = row.Cells[5].Value.ToString();

            }

            
        }

        private void Deletebtn_Click(object sender, EventArgs e)
        {
             if (Stuidcbx.Text.Trim().Length == 0 || Assidcbx.Text.Trim().Length == 0 )
            {
                MessageBox.Show("Fields Should Not Be Empty!", "User Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);

            }
            else
            {

            DialogResult result = MessageBox.Show("YOU SURE WANT TO DELETE IT?", "QUESTION", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (result == DialogResult.Yes)
            {
                var o = Configuration.getInstance().getConnection();
                cmd = new SqlCommand("DELETE  from StudentResult WHERE StudentId = '" + Stuidcbx.Text + "' AND AssessmentComponentId = '" + Assidcbx.Text + "'", o);
                cmd.ExecuteNonQuery();
                MessageBox.Show("Record Deleted successfully", "User Information", MessageBoxButtons.OK, MessageBoxIcon.Information);

                ShowData();

                Clear();
            }
            }
        }
    }
}
