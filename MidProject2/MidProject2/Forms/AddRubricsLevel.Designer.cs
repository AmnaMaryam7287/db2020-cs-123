﻿namespace MidProject2.Forms
{
    partial class AddRubricsLevel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Ridcbx = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.Rnametxt = new System.Windows.Forms.TextBox();
            this.Fullgrid = new System.Windows.Forms.DataGridView();
            this.Deletebtn = new System.Windows.Forms.Button();
            this.Updatebtn = new System.Windows.Forms.Button();
            this.Savebtn = new System.Windows.Forms.Button();
            this.C = new System.Windows.Forms.Label();
            this.lvltxt = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lvldesctxt = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Addlvlbtn = new System.Windows.Forms.Button();
            this.Addgrid = new System.Windows.Forms.DataGridView();
            this.MeasurementLevel = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Details = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Searchcbx = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.Rlvlidtxt = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.Fullgrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Addgrid)).BeginInit();
            this.SuspendLayout();
            // 
            // Ridcbx
            // 
            this.Ridcbx.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.Ridcbx.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Ridcbx.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Ridcbx.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Ridcbx.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Ridcbx.FormattingEnabled = true;
            this.Ridcbx.Location = new System.Drawing.Point(136, 51);
            this.Ridcbx.Name = "Ridcbx";
            this.Ridcbx.Size = new System.Drawing.Size(436, 28);
            this.Ridcbx.TabIndex = 0;
            this.Ridcbx.SelectedIndexChanged += new System.EventHandler(this.Ridcbx_SelectedIndexChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(23, 59);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(78, 20);
            this.label6.TabIndex = 33;
            this.label6.Text = "Rubric Id:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(23, 99);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(107, 20);
            this.label3.TabIndex = 35;
            this.label3.Text = "Rubric Name:";
            // 
            // Rnametxt
            // 
            this.Rnametxt.Enabled = false;
            this.Rnametxt.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Rnametxt.Location = new System.Drawing.Point(136, 94);
            this.Rnametxt.Name = "Rnametxt";
            this.Rnametxt.Size = new System.Drawing.Size(436, 25);
            this.Rnametxt.TabIndex = 34;
            // 
            // Fullgrid
            // 
            this.Fullgrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.Fullgrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Fullgrid.Location = new System.Drawing.Point(27, 263);
            this.Fullgrid.Name = "Fullgrid";
            this.Fullgrid.Size = new System.Drawing.Size(545, 180);
            this.Fullgrid.TabIndex = 44;
            this.Fullgrid.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.Fullgrid_CellContentClick);
            // 
            // Deletebtn
            // 
            this.Deletebtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Deletebtn.Font = new System.Drawing.Font("Century Gothic", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Deletebtn.ForeColor = System.Drawing.Color.White;
            this.Deletebtn.Location = new System.Drawing.Point(343, 449);
            this.Deletebtn.Name = "Deletebtn";
            this.Deletebtn.Size = new System.Drawing.Size(133, 54);
            this.Deletebtn.TabIndex = 43;
            this.Deletebtn.Text = "Delete";
            this.Deletebtn.UseVisualStyleBackColor = true;
            this.Deletebtn.Click += new System.EventHandler(this.Deletebtn_Click);
            // 
            // Updatebtn
            // 
            this.Updatebtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Updatebtn.Font = new System.Drawing.Font("Century Gothic", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Updatebtn.ForeColor = System.Drawing.Color.White;
            this.Updatebtn.Location = new System.Drawing.Point(218, 449);
            this.Updatebtn.Name = "Updatebtn";
            this.Updatebtn.Size = new System.Drawing.Size(119, 54);
            this.Updatebtn.TabIndex = 42;
            this.Updatebtn.Text = "Update";
            this.Updatebtn.UseVisualStyleBackColor = true;
            this.Updatebtn.Click += new System.EventHandler(this.Updatebtn_Click);
            // 
            // Savebtn
            // 
            this.Savebtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Savebtn.Font = new System.Drawing.Font("Century Gothic", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Savebtn.ForeColor = System.Drawing.Color.White;
            this.Savebtn.Location = new System.Drawing.Point(88, 449);
            this.Savebtn.Name = "Savebtn";
            this.Savebtn.Size = new System.Drawing.Size(124, 54);
            this.Savebtn.TabIndex = 40;
            this.Savebtn.Text = "Save";
            this.Savebtn.UseVisualStyleBackColor = true;
            this.Savebtn.Click += new System.EventHandler(this.Savebtn_Click);
            // 
            // C
            // 
            this.C.AutoSize = true;
            this.C.Font = new System.Drawing.Font("Arial", 19F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.C.ForeColor = System.Drawing.Color.White;
            this.C.Location = new System.Drawing.Point(190, 6);
            this.C.Name = "C";
            this.C.Size = new System.Drawing.Size(225, 31);
            this.C.TabIndex = 41;
            this.C.Text = "Add Rubrics Level";
            // 
            // lvltxt
            // 
            this.lvltxt.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lvltxt.Location = new System.Drawing.Point(136, 135);
            this.lvltxt.Name = "lvltxt";
            this.lvltxt.Size = new System.Drawing.Size(71, 25);
            this.lvltxt.TabIndex = 1;
            this.lvltxt.TextChanged += new System.EventHandler(this.lvltxt_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(23, 140);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 20);
            this.label2.TabIndex = 47;
            this.label2.Text = "Level :";
            // 
            // lvldesctxt
            // 
            this.lvldesctxt.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lvldesctxt.Location = new System.Drawing.Point(374, 135);
            this.lvldesctxt.Name = "lvldesctxt";
            this.lvldesctxt.Size = new System.Drawing.Size(198, 25);
            this.lvldesctxt.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(217, 140);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(139, 20);
            this.label1.TabIndex = 49;
            this.label1.Text = "Level Description:";
            // 
            // Addlvlbtn
            // 
            this.Addlvlbtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Addlvlbtn.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Addlvlbtn.ForeColor = System.Drawing.Color.White;
            this.Addlvlbtn.Location = new System.Drawing.Point(28, 177);
            this.Addlvlbtn.Name = "Addlvlbtn";
            this.Addlvlbtn.Size = new System.Drawing.Size(545, 42);
            this.Addlvlbtn.TabIndex = 3;
            this.Addlvlbtn.Text = "Add Level";
            this.Addlvlbtn.UseVisualStyleBackColor = true;
            this.Addlvlbtn.Click += new System.EventHandler(this.Addlvlbtn_Click);
            // 
            // Addgrid
            // 
            this.Addgrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.Addgrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Addgrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.MeasurementLevel,
            this.Details});
            this.Addgrid.Location = new System.Drawing.Point(27, 263);
            this.Addgrid.Name = "Addgrid";
            this.Addgrid.Size = new System.Drawing.Size(545, 180);
            this.Addgrid.TabIndex = 63;
            // 
            // MeasurementLevel
            // 
            this.MeasurementLevel.HeaderText = "Level";
            this.MeasurementLevel.Name = "MeasurementLevel";
            // 
            // Details
            // 
            this.Details.HeaderText = "Description";
            this.Details.Name = "Details";
            // 
            // Searchcbx
            // 
            this.Searchcbx.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.Searchcbx.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Searchcbx.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Searchcbx.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Searchcbx.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Searchcbx.FormattingEnabled = true;
            this.Searchcbx.Location = new System.Drawing.Point(183, 229);
            this.Searchcbx.Name = "Searchcbx";
            this.Searchcbx.Size = new System.Drawing.Size(389, 28);
            this.Searchcbx.TabIndex = 64;
            this.Searchcbx.SelectedIndexChanged += new System.EventHandler(this.Searchcbx_SelectedIndexChanged);
            this.Searchcbx.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Searchcbx_KeyDown);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(23, 237);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(154, 20);
            this.label4.TabIndex = 65;
            this.label4.Text = "Search By Rubric Id:";
            // 
            // Rlvlidtxt
            // 
            this.Rlvlidtxt.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Rlvlidtxt.Location = new System.Drawing.Point(30, 282);
            this.Rlvlidtxt.Name = "Rlvlidtxt";
            this.Rlvlidtxt.Size = new System.Drawing.Size(71, 25);
            this.Rlvlidtxt.TabIndex = 66;
            // 
            // AddRubricsLevel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ClientSize = new System.Drawing.Size(584, 511);
            this.Controls.Add(this.Searchcbx);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.Addlvlbtn);
            this.Controls.Add(this.lvldesctxt);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lvltxt);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.Fullgrid);
            this.Controls.Add(this.Deletebtn);
            this.Controls.Add(this.Updatebtn);
            this.Controls.Add(this.Savebtn);
            this.Controls.Add(this.C);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.Rnametxt);
            this.Controls.Add(this.Ridcbx);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.Addgrid);
            this.Controls.Add(this.Rlvlidtxt);
            this.MaximizeBox = false;
            this.Name = "AddRubricsLevel";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AddRubricsLevel";
            this.Activated += new System.EventHandler(this.AddRubricsLevel_Activated);
            this.Load += new System.EventHandler(this.AddRubricsLevel_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Fullgrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Addgrid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox Ridcbx;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox Rnametxt;
        private System.Windows.Forms.DataGridView Fullgrid;
        private System.Windows.Forms.Button Deletebtn;
        private System.Windows.Forms.Button Updatebtn;
        private System.Windows.Forms.Button Savebtn;
        private System.Windows.Forms.Label C;
        private System.Windows.Forms.TextBox lvltxt;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox lvldesctxt;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button Addlvlbtn;
        private System.Windows.Forms.DataGridView Addgrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn MeasurementLevel;
        private System.Windows.Forms.DataGridViewTextBoxColumn Details;
        private System.Windows.Forms.ComboBox Searchcbx;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox Rlvlidtxt;
    }
}