﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;


namespace MidProject2.Forms
{
    public partial class AddStudent : Form
    {

        SqlCommand cmd;
        SqlDataAdapter da;
        SqlDataReader dr;
        DataTable dt;

        public AddStudent()
        {
            InitializeComponent();
        }

        private void AddStudent_Load(object sender, EventArgs e)
        {
          //  DateTime dob = new DateTime();
            //LNametxt = DateTime.Now().ToString("d");
            var con = Configuration.getInstance().getConnection();
            //SELECT TOP 2 Name FROM Lookup ORDER BY LookupId DESC
            da = new SqlDataAdapter("SELECT * FROM Lookup WHERE Category = 'STUDENT_STATUS'", con);
            dt = new DataTable();
            da.Fill(dt);
            foreach (DataRow ROW in dt.Rows)
            {
                Statuscbx.Items.Add(ROW["Name"].ToString());

            }

            ShowData();
        }

        private void AddStudent_Activated(object sender, EventArgs e)
        {
            FNametxt.Focus();
        }

        private void Savebtn_Click(object sender, EventArgs e)
        {

            if (RegNotxt.Text.Trim().Length == 0 || FNametxt.Text.Trim().Length == 0 || LNametxt.Text.Trim().Length == 0 || Contacttxt.Text.Trim().Length == 0 || Emailtxt.Text.Trim().Length == 0 || LUpIdtxt.Text.Trim().Length == 0 || LUpIdtxt.Text.Trim().Length == 0)
            {
                MessageBox.Show("Fields Should Not Be Empty!", "User Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);

            }

            else
            {

                if (!this.Emailtxt.Text.Contains('.') || !this.Emailtxt.Text.Contains('u') || !this.Emailtxt.Text.Contains('e') || !this.Emailtxt.Text.Contains('t') )
                {
                    MessageBox.Show("Enter A Valid Email", "User Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                }

                else
                {






                    var con = Configuration.getInstance().getConnection();
                    cmd = new SqlCommand("INSERT INTO Student (FirstName, LastName,Contact,Email,RegistrationNumber,Status) VALUES (@FirstName, @LastName,@Contact,@Email,@RegistrationNumber,@Status)", con);
                    cmd.Parameters.AddWithValue("@FirstName", FNametxt.Text);
                    cmd.Parameters.AddWithValue("@LastName", LNametxt.Text);
                    cmd.Parameters.AddWithValue("@Contact", Contacttxt.Text);
                    cmd.Parameters.AddWithValue("@Email", Emailtxt.Text);
                    cmd.Parameters.AddWithValue("@RegistrationNumber", RegNotxt.Text);
                    cmd.Parameters.AddWithValue("@Status", LUpIdtxt.Text);
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Successfully saved", "User Information", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    ShowData();
                    Clear();
                    FNametxt.Focus();
                }

            }
        }

       

        public void ShowData()
        {
            
            var oo = Configuration.getInstance().getConnection();
            cmd = new SqlCommand("Select   Student.RegistrationNumber AS RegNo, CONCAT(Student.FirstName,Student.LastName) AS FullName ,Student.Contact,Student.Email,Lookup.Name AS Status , Student.Id from Student INNER JOIN Lookup ON Student.Status = Lookup.LookupId ORDER BY Student.RegistrationNumber ASC", oo);
            da = new SqlDataAdapter(cmd);
            dt = new DataTable();
            da.Fill(dt);
            Fullgrid.DataSource = dt;
        }

        public void Clear()
        {
            foreach (var item in this.Controls)
            {
                if (item.GetType().Equals(typeof(TextBox)))
                {
                    TextBox t1 = item as TextBox;
                    t1.Text = string.Empty;
                }

                if (item.GetType().Equals(typeof(ComboBox)))
                {
                    ComboBox c1 = item as ComboBox;
                    c1.Text = null;
                }

            }
        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void Statuscbx_SelectedIndexChanged(object sender, EventArgs e)
        {
            var cc = Configuration.getInstance().getConnection();
            dt = new DataTable();
            //con5.Open();
            dr = null;
            cmd = new SqlCommand("SELECT * FROM Lookup WHERE Name = '" + Statuscbx.Text + "'", cc);
            dr = cmd.ExecuteReader();

            while (dr.Read())
            {
                LUpIdtxt.Text = dr["LookupId"].ToString();

            }
            dr.Close();
        }

        private void Updatebtn_Click(object sender, EventArgs e)
        {
            if (RegNotxt.Text.Trim().Length == 0 || FNametxt.Text.Trim().Length == 0 || LNametxt.Text.Trim().Length == 0 || Contacttxt.Text.Trim().Length == 0 || Emailtxt.Text.Trim().Length == 0 || LUpIdtxt.Text.Trim().Length == 0 || LUpIdtxt.Text.Trim().Length == 0)
            {
                MessageBox.Show("Fields Should Not Be Empty!", "User Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);

            }

            else
            {

                var nn = Configuration.getInstance().getConnection();
                string query = "UPDATE Student set RegistrationNumber = '" + RegNotxt.Text + "' , FirstName = '" + FNametxt.Text + "' , LastName = '" + LNametxt.Text + "'  , Contact = '" + Contacttxt.Text + "' , Email = '" + Emailtxt.Text + "' , Status = '" + LUpIdtxt.Text + "'  WHERE Id ='" + Stuidtxt.Text + "'";
                cmd = new SqlCommand(query, nn);
                //SqlDataReader dr;
                try
                {
                    //con.Open();
                    dr = cmd.ExecuteReader();
                    MessageBox.Show("Record Updated successfully", "User Information", MessageBoxButtons.OK, MessageBoxIcon.Information);


                    while (dr.Read())
                    {
                    }
                    //con.Close();
                    dr.Close();
                }
                catch (Exception ec)
                {
                    MessageBox.Show(ec.Message);
                }

                ShowData();

                Clear();
            }
            

        }

        private void Deletebtn_Click(object sender, EventArgs e)
        {
            if (RegNotxt.Text.Trim().Length == 0 || FNametxt.Text.Trim().Length == 0 || LNametxt.Text.Trim().Length == 0 || Contacttxt.Text.Trim().Length == 0 || Emailtxt.Text.Trim().Length == 0 || LUpIdtxt.Text.Trim().Length == 0 || LUpIdtxt.Text.Trim().Length == 0)
            {
                MessageBox.Show("Fields Should Not Be Empty!", "User Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);

            }

            else
            {

                DialogResult result = MessageBox.Show("YOU SURE WANT TO DELETE IT?", "QUESTION", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (result == DialogResult.Yes)
                {
                    var o = Configuration.getInstance().getConnection();
                    cmd = new SqlCommand("DELETE  from Student WHERE Id ='" + Stuidtxt.Text + "'", o);
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Record Deleted successfully", "User Information", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    ShowData();

                    Clear();
                }
            }
        }

        private void Fullgrid_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                //gets a collection that contains all the rows
                DataGridViewRow row = this.Fullgrid.Rows[e.RowIndex];
                RegNotxt.Text = row.Cells[0].Value.ToString();
                //textBox1.Text = row.Cells[1].Value.ToString();

                Contacttxt.Text = row.Cells[2].Value.ToString();
                Emailtxt.Text = row.Cells[3].Value.ToString();
                Statuscbx.Text = row.Cells[4].Value.ToString();
                Stuidtxt.Text = row.Cells[5].Value.ToString();

                var x = Configuration.getInstance().getConnection();
                DataTable dt1 = new DataTable();
                //con5.Open();
                SqlDataReader dr1 = null;
                SqlCommand cmd1 = new SqlCommand("SELECT * FROM Student WHERE RegistrationNumber = '" + RegNotxt.Text + "'", x);
                dr1 = cmd1.ExecuteReader();

                while (dr1.Read())
                {
                    FNametxt.Text = dr1["FirstName"].ToString();
                    LNametxt.Text = dr1["LastName"].ToString();

                }
                dr1.Close();

                

            }
        }

        private void Statuscbx_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void Contacttxt_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&  (e.KeyChar != '.'))
  {
              e.Handled = true;
  }
        }

        private void RegNotxt_TextChanged(object sender, EventArgs e)
        {

        }

        private void Searchtxt_TextChanged(object sender, EventArgs e)
        {
            if (Searchtxt.Text != "")
            {


                var n = Configuration.getInstance().getConnection();
                cmd = new SqlCommand("Select Student.RegistrationNumber AS RegNo, CONCAT(Student.FirstName, Student.LastName) AS FullName ,Student.Contact,Student.Email,Lookup.Name AS Status from Student INNER JOIN Lookup ON Student.Status = Lookup.LookupId WHERE RegistrationNumber = '" + Searchtxt.Text + "' OR FirstName = '" + Searchtxt.Text + "'", n);
                da = new SqlDataAdapter(cmd);
                dt = new DataTable();
                da.Fill(dt);
                Fullgrid.DataSource = dt;
            }
            else
            {
                ShowData();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var StudentList = new Forms.Student_List();
            StudentList.Show();
        }

        private void Emailtxt_TextChanged(object sender, EventArgs e)
        {
            
        }
    }
}
