﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using Microsoft.Reporting.WinForms;

namespace MidProject2.Forms
{
    public partial class RubricsList : Form
    {

        SqlDataAdapter da;

        DataTable dt;

        public RubricsList()
        {
            InitializeComponent();
        }

        private void RubricsList_Load(object sender, EventArgs e)
        {

            var n = Configuration.getInstance().getConnection();
            da = new SqlDataAdapter("Select Clo.Id AS CLOId, Clo.Name AS CLOName, Rubric.Id AS RubricId,Rubric.Details AS RubricDetail, CONCAT( Rubric.CloId , Clo.Name ) AS CloIdName  from Clo LEFT JOIN Rubric ON Clo.Id = Rubric.CloId", n);
            dt = new DataTable();
            da.Fill(dt);
            reportViewer1.LocalReport.DataSources.Clear();
            ReportDataSource rds = new ReportDataSource("DataSet3", dt);
            reportViewer1.LocalReport.ReportPath = @"C:\Users\BestBoi\Desktop\LATEST\MidProject2\MidProject2\Reports\CLO Wise Rubrics List.rdlc";

            reportViewer1.LocalReport.DataSources.Add(rds);
            this.reportViewer1.RefreshReport();
        }
    }
}
