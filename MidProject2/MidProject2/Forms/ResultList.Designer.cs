﻿namespace MidProject2.Forms
{
    partial class ResultList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.maxlvltxt = new System.Windows.Forms.TextBox();
            this.Typecbx = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Loadrptbtn = new System.Windows.Forms.Button();
            this.reportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.SuspendLayout();
            // 
            // maxlvltxt
            // 
            this.maxlvltxt.Location = new System.Drawing.Point(287, 262);
            this.maxlvltxt.Name = "maxlvltxt";
            this.maxlvltxt.Size = new System.Drawing.Size(100, 20);
            this.maxlvltxt.TabIndex = 91;
            // 
            // Typecbx
            // 
            this.Typecbx.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.Typecbx.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Typecbx.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Typecbx.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Typecbx.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Typecbx.FormattingEnabled = true;
            this.Typecbx.Items.AddRange(new object[] {
            "Assessment Wise",
            "CLO Wise"});
            this.Typecbx.Location = new System.Drawing.Point(122, 6);
            this.Typecbx.Name = "Typecbx";
            this.Typecbx.Size = new System.Drawing.Size(368, 28);
            this.Typecbx.TabIndex = 92;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(99, 20);
            this.label1.TabIndex = 93;
            this.label1.Text = "Report Type:";
            // 
            // Loadrptbtn
            // 
            this.Loadrptbtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Loadrptbtn.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Loadrptbtn.ForeColor = System.Drawing.Color.White;
            this.Loadrptbtn.Location = new System.Drawing.Point(496, 6);
            this.Loadrptbtn.Name = "Loadrptbtn";
            this.Loadrptbtn.Size = new System.Drawing.Size(166, 30);
            this.Loadrptbtn.TabIndex = 94;
            this.Loadrptbtn.Text = "Load Report";
            this.Loadrptbtn.UseVisualStyleBackColor = true;
            this.Loadrptbtn.Click += new System.EventHandler(this.Loadrptbtn_Click);
            // 
            // reportViewer1
            // 
            this.reportViewer1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.reportViewer1.Location = new System.Drawing.Point(0, 42);
            this.reportViewer1.Name = "reportViewer1";
            this.reportViewer1.Size = new System.Drawing.Size(674, 503);
            this.reportViewer1.TabIndex = 2;
            // 
            // ResultList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ClientSize = new System.Drawing.Size(674, 545);
            this.Controls.Add(this.Loadrptbtn);
            this.Controls.Add(this.Typecbx);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.reportViewer1);
            this.Controls.Add(this.maxlvltxt);
            this.MaximizeBox = false;
            this.Name = "ResultList";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ResultList";
            this.Load += new System.EventHandler(this.ResultList_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox maxlvltxt;
        private System.Windows.Forms.ComboBox Typecbx;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button Loadrptbtn;
        private Microsoft.Reporting.WinForms.ReportViewer reportViewer1;
    }
}