﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;
using System.Data.SqlClient;

namespace MidProject2.Forms
{
    public partial class Student_List : Form
    {

       
        SqlDataAdapter da;
   
        DataTable dt;
        public Student_List()
        {
            InitializeComponent();
        }

        private void Student_List_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'projectBDataSet1.Student' table. You can move, or remove it, as needed.
            //this.studentTableAdapter.Fill(this.projectBDataSet1.Student);
            // TODO: This line of code loads data into the 'projectBDataSet1.Student' table. You can move, or remove it, as needed.
            //this.studentTableAdapter1.Fill(this.projectBDataSet1.Student);
            // TODO: This line of code loads data into the 'projectBDataSet.Student' table. You can move, or remove it, as needed.
            //this.studentTableAdapter.Fill(this.projectBDataSet.Student);


            var n = Configuration.getInstance().getConnection();
            //cmd = new SqlCommand("Select Student.RegistrationNumber AS RegNo, CONCAT(Student.FirstName,Student.LastName) AS FullName ,Student.Contact,Student.Email,Lookup.Name AS Status from Student INNER JOIN Lookup ON Student.Status = Lookup.LookupId", n);
            da = new SqlDataAdapter("Select Student.RegistrationNumber ,CONCAT(Student.FirstName,Student.LastName) AS FullName,Student.Contact,Student.Email,Lookup.Name AS Status from Student INNER JOIN Lookup ON Student.Status = Lookup.LookupId", n);
            //da = new SqlDataAdapter(cmd);
            dt = new DataTable();
            //DataSet ds = new DataSet();
            //ProjectBDataSet1 ds = new ProjectBDataSet1();
            da.Fill(dt);
            reportViewer1.LocalReport.DataSources.Clear();
            ReportDataSource rds = new ReportDataSource("DataSet1",dt);
            reportViewer1.LocalReport.ReportPath = @"C:\Users\BestBoi\Desktop\LATEST\MidProject2\MidProject2\Reports\Student List.rdlc";
            
            reportViewer1.LocalReport.DataSources.Add(rds);
            this.reportViewer1.RefreshReport();
            
        }
    }
}
