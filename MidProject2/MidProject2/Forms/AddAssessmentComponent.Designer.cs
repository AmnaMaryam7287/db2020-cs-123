﻿namespace MidProject2.Forms
{
    partial class AddAssessmentComponent
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.Cmarkstxt = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Cnametxt = new System.Windows.Forms.TextBox();
            this.C = new System.Windows.Forms.Label();
            this.Fullgrid = new System.Windows.Forms.DataGridView();
            this.Deletebtn = new System.Windows.Forms.Button();
            this.Updatebtn = new System.Windows.Forms.Button();
            this.Savebtn = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.Titletxt = new System.Windows.Forms.TextBox();
            this.Assidcbx = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.Ridcbx = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.Rnametxt = new System.Windows.Forms.TextBox();
            this.Searchcbx = new System.Windows.Forms.ComboBox();
            this.AssCompIdtxt = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.Fullgrid)).BeginInit();
            this.SuspendLayout();
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(12, 254);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(180, 20);
            this.label4.TabIndex = 71;
            this.label4.Text = "Search By Ass.Comp.Id:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(44, 224);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(149, 20);
            this.label2.TabIndex = 70;
            this.label2.Text = "Component Marks:";
            // 
            // Cmarkstxt
            // 
            this.Cmarkstxt.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cmarkstxt.Location = new System.Drawing.Point(203, 224);
            this.Cmarkstxt.Name = "Cmarkstxt";
            this.Cmarkstxt.Size = new System.Drawing.Size(594, 25);
            this.Cmarkstxt.TabIndex = 3;
            this.Cmarkstxt.TextChanged += new System.EventHandler(this.Cmarkstxt_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(44, 193);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(149, 20);
            this.label1.TabIndex = 69;
            this.label1.Text = "Component Name:";
            // 
            // Cnametxt
            // 
            this.Cnametxt.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cnametxt.Location = new System.Drawing.Point(202, 193);
            this.Cnametxt.Name = "Cnametxt";
            this.Cnametxt.Size = new System.Drawing.Size(594, 25);
            this.Cnametxt.TabIndex = 2;
            // 
            // C
            // 
            this.C.AutoSize = true;
            this.C.Font = new System.Drawing.Font("Arial", 19F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.C.ForeColor = System.Drawing.Color.White;
            this.C.Location = new System.Drawing.Point(275, 9);
            this.C.Name = "C";
            this.C.Size = new System.Drawing.Size(347, 31);
            this.C.TabIndex = 68;
            this.C.Text = "Add Assessment Component";
            // 
            // Fullgrid
            // 
            this.Fullgrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.Fullgrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Fullgrid.Location = new System.Drawing.Point(47, 285);
            this.Fullgrid.Name = "Fullgrid";
            this.Fullgrid.Size = new System.Drawing.Size(751, 256);
            this.Fullgrid.TabIndex = 67;
            this.Fullgrid.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.Fullgrid_CellContentClick);
            // 
            // Deletebtn
            // 
            this.Deletebtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Deletebtn.Font = new System.Drawing.Font("Century Gothic", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Deletebtn.ForeColor = System.Drawing.Color.White;
            this.Deletebtn.Location = new System.Drawing.Point(489, 547);
            this.Deletebtn.Name = "Deletebtn";
            this.Deletebtn.Size = new System.Drawing.Size(133, 54);
            this.Deletebtn.TabIndex = 66;
            this.Deletebtn.Text = "Delete";
            this.Deletebtn.UseVisualStyleBackColor = true;
            this.Deletebtn.Click += new System.EventHandler(this.Deletebtn_Click);
            // 
            // Updatebtn
            // 
            this.Updatebtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Updatebtn.Font = new System.Drawing.Font("Century Gothic", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Updatebtn.ForeColor = System.Drawing.Color.White;
            this.Updatebtn.Location = new System.Drawing.Point(331, 547);
            this.Updatebtn.Name = "Updatebtn";
            this.Updatebtn.Size = new System.Drawing.Size(119, 54);
            this.Updatebtn.TabIndex = 65;
            this.Updatebtn.Text = "Update";
            this.Updatebtn.UseVisualStyleBackColor = true;
            this.Updatebtn.Click += new System.EventHandler(this.Updatebtn_Click);
            // 
            // Savebtn
            // 
            this.Savebtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Savebtn.Font = new System.Drawing.Font("Century Gothic", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Savebtn.ForeColor = System.Drawing.Color.White;
            this.Savebtn.Location = new System.Drawing.Point(158, 547);
            this.Savebtn.Name = "Savebtn";
            this.Savebtn.Size = new System.Drawing.Size(124, 54);
            this.Savebtn.TabIndex = 4;
            this.Savebtn.Text = "Save";
            this.Savebtn.UseVisualStyleBackColor = true;
            this.Savebtn.Click += new System.EventHandler(this.Savebtn_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(44, 99);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(125, 20);
            this.label3.TabIndex = 64;
            this.label3.Text = "Assessment Title:";
            // 
            // Titletxt
            // 
            this.Titletxt.Enabled = false;
            this.Titletxt.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Titletxt.Location = new System.Drawing.Point(203, 94);
            this.Titletxt.Name = "Titletxt";
            this.Titletxt.Size = new System.Drawing.Size(594, 25);
            this.Titletxt.TabIndex = 59;
            // 
            // Assidcbx
            // 
            this.Assidcbx.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.Assidcbx.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Assidcbx.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Assidcbx.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Assidcbx.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Assidcbx.FormattingEnabled = true;
            this.Assidcbx.Location = new System.Drawing.Point(202, 60);
            this.Assidcbx.Name = "Assidcbx";
            this.Assidcbx.Size = new System.Drawing.Size(595, 28);
            this.Assidcbx.TabIndex = 0;
            this.Assidcbx.SelectedIndexChanged += new System.EventHandler(this.Assidcbx_SelectedIndexChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(44, 68);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(114, 20);
            this.label6.TabIndex = 74;
            this.label6.Text = "Assessment Id:";
            // 
            // Ridcbx
            // 
            this.Ridcbx.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.Ridcbx.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Ridcbx.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Ridcbx.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Ridcbx.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Ridcbx.FormattingEnabled = true;
            this.Ridcbx.Location = new System.Drawing.Point(203, 125);
            this.Ridcbx.Name = "Ridcbx";
            this.Ridcbx.Size = new System.Drawing.Size(595, 28);
            this.Ridcbx.TabIndex = 1;
            this.Ridcbx.SelectedIndexChanged += new System.EventHandler(this.Ridcbx_SelectedIndexChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(45, 133);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(78, 20);
            this.label5.TabIndex = 78;
            this.label5.Text = "Rubric Id:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(45, 164);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(107, 20);
            this.label7.TabIndex = 76;
            this.label7.Text = "Rubric Name:";
            // 
            // Rnametxt
            // 
            this.Rnametxt.Enabled = false;
            this.Rnametxt.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Rnametxt.Location = new System.Drawing.Point(204, 159);
            this.Rnametxt.Name = "Rnametxt";
            this.Rnametxt.Size = new System.Drawing.Size(594, 25);
            this.Rnametxt.TabIndex = 75;
            // 
            // Searchcbx
            // 
            this.Searchcbx.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.Searchcbx.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Searchcbx.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Searchcbx.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Searchcbx.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Searchcbx.FormattingEnabled = true;
            this.Searchcbx.Location = new System.Drawing.Point(202, 254);
            this.Searchcbx.Name = "Searchcbx";
            this.Searchcbx.Size = new System.Drawing.Size(595, 28);
            this.Searchcbx.TabIndex = 79;
            this.Searchcbx.SelectedIndexChanged += new System.EventHandler(this.Searchcbx_SelectedIndexChanged);
            this.Searchcbx.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Searchcbx_KeyDown);
            // 
            // AssCompIdtxt
            // 
            this.AssCompIdtxt.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AssCompIdtxt.Location = new System.Drawing.Point(49, 298);
            this.AssCompIdtxt.Name = "AssCompIdtxt";
            this.AssCompIdtxt.Size = new System.Drawing.Size(61, 25);
            this.AssCompIdtxt.TabIndex = 80;
            // 
            // AddAssessmentComponent
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ClientSize = new System.Drawing.Size(810, 613);
            this.Controls.Add(this.Searchcbx);
            this.Controls.Add(this.Ridcbx);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.Rnametxt);
            this.Controls.Add(this.Assidcbx);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.Cmarkstxt);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Cnametxt);
            this.Controls.Add(this.C);
            this.Controls.Add(this.Fullgrid);
            this.Controls.Add(this.Deletebtn);
            this.Controls.Add(this.Updatebtn);
            this.Controls.Add(this.Savebtn);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.Titletxt);
            this.Controls.Add(this.AssCompIdtxt);
            this.MaximizeBox = false;
            this.Name = "AddAssessmentComponent";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AddAssessmentComponent";
            this.Load += new System.EventHandler(this.AddAssessmentComponent_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Fullgrid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox Cmarkstxt;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox Cnametxt;
        private System.Windows.Forms.Label C;
        private System.Windows.Forms.DataGridView Fullgrid;
        private System.Windows.Forms.Button Deletebtn;
        private System.Windows.Forms.Button Updatebtn;
        private System.Windows.Forms.Button Savebtn;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox Titletxt;
        private System.Windows.Forms.ComboBox Assidcbx;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox Ridcbx;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox Rnametxt;
        private System.Windows.Forms.ComboBox Searchcbx;
        private System.Windows.Forms.TextBox AssCompIdtxt;
    }
}