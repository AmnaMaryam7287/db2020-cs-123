﻿namespace MidProject2.Forms
{
    partial class Attendance
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.FullNametxt = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Stuidcbx = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.Attendancecbx = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.StuRegtxt = new System.Windows.Forms.TextBox();
            this.Fullgrid = new System.Windows.Forms.DataGridView();
            this.button1 = new System.Windows.Forms.Button();
            this.Updatebtn = new System.Windows.Forms.Button();
            this.Deletebtn = new System.Windows.Forms.Button();
            this.Savebtn = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.LUpIdtxt = new System.Windows.Forms.TextBox();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.AttIdtxt = new System.Windows.Forms.TextBox();
            this.Searchtxt = new System.Windows.Forms.TextBox();
            this.Timetxt = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.Fullgrid)).BeginInit();
            this.SuspendLayout();
            // 
            // FullNametxt
            // 
            this.FullNametxt.Enabled = false;
            this.FullNametxt.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FullNametxt.Location = new System.Drawing.Point(159, 109);
            this.FullNametxt.Name = "FullNametxt";
            this.FullNametxt.Size = new System.Drawing.Size(431, 25);
            this.FullNametxt.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(14, 111);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(116, 20);
            this.label1.TabIndex = 3;
            this.label1.Text = "Student Name:";
            // 
            // Stuidcbx
            // 
            this.Stuidcbx.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.Stuidcbx.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Stuidcbx.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Stuidcbx.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Stuidcbx.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Stuidcbx.FormattingEnabled = true;
            this.Stuidcbx.Location = new System.Drawing.Point(159, 75);
            this.Stuidcbx.Name = "Stuidcbx";
            this.Stuidcbx.Size = new System.Drawing.Size(431, 28);
            this.Stuidcbx.TabIndex = 12;
            this.Stuidcbx.SelectedIndexChanged += new System.EventHandler(this.Stuidcbx_SelectedIndexChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(14, 78);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(87, 20);
            this.label6.TabIndex = 13;
            this.label6.Text = "Student Id:";
            // 
            // Attendancecbx
            // 
            this.Attendancecbx.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.Attendancecbx.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Attendancecbx.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Attendancecbx.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Attendancecbx.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Attendancecbx.FormattingEnabled = true;
            this.Attendancecbx.Location = new System.Drawing.Point(159, 171);
            this.Attendancecbx.Name = "Attendancecbx";
            this.Attendancecbx.Size = new System.Drawing.Size(431, 28);
            this.Attendancecbx.TabIndex = 14;
            this.Attendancecbx.SelectedIndexChanged += new System.EventHandler(this.Attendancecbx_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(14, 177);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(102, 20);
            this.label2.TabIndex = 15;
            this.label2.Text = "Attendance:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(14, 144);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(112, 20);
            this.label3.TabIndex = 17;
            this.label3.Text = "Student Reg#:";
            // 
            // StuRegtxt
            // 
            this.StuRegtxt.Enabled = false;
            this.StuRegtxt.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.StuRegtxt.Location = new System.Drawing.Point(159, 140);
            this.StuRegtxt.Name = "StuRegtxt";
            this.StuRegtxt.Size = new System.Drawing.Size(431, 25);
            this.StuRegtxt.TabIndex = 16;
            // 
            // Fullgrid
            // 
            this.Fullgrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.Fullgrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Fullgrid.Location = new System.Drawing.Point(12, 231);
            this.Fullgrid.Name = "Fullgrid";
            this.Fullgrid.Size = new System.Drawing.Size(578, 174);
            this.Fullgrid.TabIndex = 18;
            this.Fullgrid.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.Fullgrid_CellContentClick);
            // 
            // button1
            // 
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Century Gothic", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(433, 411);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(157, 66);
            this.button1.TabIndex = 24;
            this.button1.Text = "Print Attendance List";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Updatebtn
            // 
            this.Updatebtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Updatebtn.Font = new System.Drawing.Font("Century Gothic", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Updatebtn.ForeColor = System.Drawing.Color.White;
            this.Updatebtn.Location = new System.Drawing.Point(160, 411);
            this.Updatebtn.Name = "Updatebtn";
            this.Updatebtn.Size = new System.Drawing.Size(142, 66);
            this.Updatebtn.TabIndex = 23;
            this.Updatebtn.Text = "Update";
            this.Updatebtn.UseVisualStyleBackColor = true;
            this.Updatebtn.Click += new System.EventHandler(this.Updatebtn_Click);
            // 
            // Deletebtn
            // 
            this.Deletebtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Deletebtn.Font = new System.Drawing.Font("Century Gothic", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Deletebtn.ForeColor = System.Drawing.Color.White;
            this.Deletebtn.Location = new System.Drawing.Point(308, 411);
            this.Deletebtn.Name = "Deletebtn";
            this.Deletebtn.Size = new System.Drawing.Size(119, 66);
            this.Deletebtn.TabIndex = 22;
            this.Deletebtn.Text = "Delete";
            this.Deletebtn.UseVisualStyleBackColor = true;
            this.Deletebtn.Click += new System.EventHandler(this.Deletebtn_Click);
            // 
            // Savebtn
            // 
            this.Savebtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Savebtn.Font = new System.Drawing.Font("Century Gothic", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Savebtn.ForeColor = System.Drawing.Color.White;
            this.Savebtn.Location = new System.Drawing.Point(12, 411);
            this.Savebtn.Name = "Savebtn";
            this.Savebtn.Size = new System.Drawing.Size(142, 66);
            this.Savebtn.TabIndex = 21;
            this.Savebtn.Text = "Save";
            this.Savebtn.UseVisualStyleBackColor = true;
            this.Savebtn.Click += new System.EventHandler(this.Savebtn_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 19F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(180, 9);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(237, 31);
            this.label7.TabIndex = 25;
            this.label7.Text = "Student Attendance";
            // 
            // LUpIdtxt
            // 
            this.LUpIdtxt.Location = new System.Drawing.Point(12, 255);
            this.LUpIdtxt.Name = "LUpIdtxt";
            this.LUpIdtxt.Size = new System.Drawing.Size(67, 20);
            this.LUpIdtxt.TabIndex = 26;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker1.Location = new System.Drawing.Point(159, 204);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(431, 25);
            this.dateTimePicker1.TabIndex = 27;
            this.dateTimePicker1.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(16, 205);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(137, 20);
            this.label4.TabIndex = 28;
            this.label4.Text = "Search Datewise:";
            // 
            // AttIdtxt
            // 
            this.AttIdtxt.Location = new System.Drawing.Point(12, 281);
            this.AttIdtxt.Name = "AttIdtxt";
            this.AttIdtxt.Size = new System.Drawing.Size(67, 20);
            this.AttIdtxt.TabIndex = 29;
            // 
            // Searchtxt
            // 
            this.Searchtxt.Location = new System.Drawing.Point(343, 302);
            this.Searchtxt.Name = "Searchtxt";
            this.Searchtxt.Size = new System.Drawing.Size(167, 20);
            this.Searchtxt.TabIndex = 31;
            // 
            // Timetxt
            // 
            this.Timetxt.Location = new System.Drawing.Point(319, 255);
            this.Timetxt.Name = "Timetxt";
            this.Timetxt.Size = new System.Drawing.Size(43, 20);
            this.Timetxt.TabIndex = 32;
            this.Timetxt.Text = "12:00:00 AM";
            // 
            // Attendance
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ClientSize = new System.Drawing.Size(602, 485);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.Updatebtn);
            this.Controls.Add(this.Deletebtn);
            this.Controls.Add(this.Savebtn);
            this.Controls.Add(this.Fullgrid);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.StuRegtxt);
            this.Controls.Add(this.Attendancecbx);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.Stuidcbx);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.FullNametxt);
            this.Controls.Add(this.AttIdtxt);
            this.Controls.Add(this.LUpIdtxt);
            this.Controls.Add(this.Searchtxt);
            this.Controls.Add(this.Timetxt);
            this.MaximizeBox = false;
            this.Name = "Attendance";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Attendance";
            this.Activated += new System.EventHandler(this.Attendance_Activated);
            this.Load += new System.EventHandler(this.Attendance_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Fullgrid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox FullNametxt;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox Stuidcbx;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox Attendancecbx;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox StuRegtxt;
        private System.Windows.Forms.DataGridView Fullgrid;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button Updatebtn;
        private System.Windows.Forms.Button Deletebtn;
        private System.Windows.Forms.Button Savebtn;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox LUpIdtxt;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox AttIdtxt;
        private System.Windows.Forms.TextBox Searchtxt;
        private System.Windows.Forms.TextBox Timetxt;
    }
}