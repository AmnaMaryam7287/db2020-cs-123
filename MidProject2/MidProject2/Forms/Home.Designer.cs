﻿namespace MidProject2.Forms
{
    partial class Home
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Addclobtn = new System.Windows.Forms.Button();
            this.Rubricbtn = new System.Windows.Forms.Button();
            this.AddAssesment = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.Attendance = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.button5 = new System.Windows.Forms.Button();
            this.Addstudentbtn = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // Addclobtn
            // 
            this.Addclobtn.BackColor = System.Drawing.Color.White;
            this.Addclobtn.FlatAppearance.BorderSize = 0;
            this.Addclobtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Addclobtn.Font = new System.Drawing.Font("Impact", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Addclobtn.Location = new System.Drawing.Point(3, 138);
            this.Addclobtn.Name = "Addclobtn";
            this.Addclobtn.Size = new System.Drawing.Size(211, 53);
            this.Addclobtn.TabIndex = 0;
            this.Addclobtn.Text = "CLO Registration";
            this.Addclobtn.UseVisualStyleBackColor = false;
            this.Addclobtn.Click += new System.EventHandler(this.Addclobtn_Click);
            // 
            // Rubricbtn
            // 
            this.Rubricbtn.BackColor = System.Drawing.Color.White;
            this.Rubricbtn.FlatAppearance.BorderSize = 0;
            this.Rubricbtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Rubricbtn.Font = new System.Drawing.Font("Impact", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Rubricbtn.Location = new System.Drawing.Point(3, 197);
            this.Rubricbtn.Name = "Rubricbtn";
            this.Rubricbtn.Size = new System.Drawing.Size(211, 53);
            this.Rubricbtn.TabIndex = 2;
            this.Rubricbtn.Text = "Add Rubrics";
            this.Rubricbtn.UseVisualStyleBackColor = false;
            this.Rubricbtn.Click += new System.EventHandler(this.Rubricbtn_Click);
            // 
            // AddAssesment
            // 
            this.AddAssesment.BackColor = System.Drawing.Color.White;
            this.AddAssesment.FlatAppearance.BorderSize = 0;
            this.AddAssesment.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.AddAssesment.Font = new System.Drawing.Font("Impact", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AddAssesment.Location = new System.Drawing.Point(597, 256);
            this.AddAssesment.Name = "AddAssesment";
            this.AddAssesment.Size = new System.Drawing.Size(211, 53);
            this.AddAssesment.TabIndex = 3;
            this.AddAssesment.Text = "Add Assessment";
            this.AddAssesment.UseVisualStyleBackColor = false;
            this.AddAssesment.Click += new System.EventHandler(this.AddAssesment_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.White;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Impact", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(310, 359);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(211, 53);
            this.button2.TabIndex = 4;
            this.button2.Text = "Add Rubrics Level";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // Attendance
            // 
            this.Attendance.BackColor = System.Drawing.Color.White;
            this.Attendance.FlatAppearance.BorderSize = 0;
            this.Attendance.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Attendance.Font = new System.Drawing.Font("Impact", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Attendance.Location = new System.Drawing.Point(3, 256);
            this.Attendance.Name = "Attendance";
            this.Attendance.Size = new System.Drawing.Size(211, 53);
            this.Attendance.TabIndex = 5;
            this.Attendance.Text = "Attendance";
            this.Attendance.UseVisualStyleBackColor = false;
            this.Attendance.Click += new System.EventHandler(this.Attendance_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.White;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Impact", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(597, 79);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(211, 53);
            this.button1.TabIndex = 6;
            this.button1.Text = "Add Assessment Component";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.White;
            this.button3.FlatAppearance.BorderSize = 0;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Font = new System.Drawing.Font("Impact", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Location = new System.Drawing.Point(597, 138);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(211, 53);
            this.button3.TabIndex = 7;
            this.button3.Text = "Student Evaluation";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.White;
            this.button4.FlatAppearance.BorderSize = 0;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Font = new System.Drawing.Font("Impact", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.Location = new System.Drawing.Point(597, 197);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(211, 53);
            this.button4.TabIndex = 8;
            this.button4.Text = "Student Result";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::MidProject2.Properties.Resources._13038;
            this.pictureBox1.Location = new System.Drawing.Point(260, 90);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(308, 263);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 9;
            this.pictureBox1.TabStop = false;
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.White;
            this.button5.Font = new System.Drawing.Font("Impact", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.Location = new System.Drawing.Point(254, 12);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(314, 59);
            this.button5.TabIndex = 10;
            this.button5.Text = "Student Management System";
            this.button5.UseVisualStyleBackColor = false;
            // 
            // Addstudentbtn
            // 
            this.Addstudentbtn.BackColor = System.Drawing.Color.White;
            this.Addstudentbtn.FlatAppearance.BorderSize = 0;
            this.Addstudentbtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Addstudentbtn.Font = new System.Drawing.Font("Impact", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Addstudentbtn.Location = new System.Drawing.Point(3, 79);
            this.Addstudentbtn.Name = "Addstudentbtn";
            this.Addstudentbtn.Size = new System.Drawing.Size(211, 53);
            this.Addstudentbtn.TabIndex = 1;
            this.Addstudentbtn.Text = "Student Registration";
            this.Addstudentbtn.UseVisualStyleBackColor = false;
            this.Addstudentbtn.Click += new System.EventHandler(this.Addstudentbtn_Click);
            // 
            // Home
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Teal;
            this.ClientSize = new System.Drawing.Size(820, 424);
            this.Controls.Add(this.Addstudentbtn);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.Attendance);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.AddAssesment);
            this.Controls.Add(this.Rubricbtn);
            this.Controls.Add(this.Addclobtn);
            this.Name = "Home";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Home";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button Addclobtn;
        private System.Windows.Forms.Button Rubricbtn;
        private System.Windows.Forms.Button AddAssesment;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button Attendance;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button Addstudentbtn;
    }
}