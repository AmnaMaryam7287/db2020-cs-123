﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MidProject2.Forms
{
    public partial class Home : Form
    {
        public Home()
        {
            InitializeComponent();
        }

        private void Addstudentbtn_Click(object sender, EventArgs e)
        {
            var AddStudent = new Forms.AddStudent();
            AddStudent.Show();
        }

        private void Attendance_Click(object sender, EventArgs e)
        {
            var Attendance = new Forms.Attendance();
            Attendance.Show();
        }

        private void Addclobtn_Click(object sender, EventArgs e)
        {
            var AddCLOs = new Forms.AddCLOs();
            AddCLOs.Show();
        }

        private void Rubricbtn_Click(object sender, EventArgs e)
        {
            var AddRubrics = new Forms.AddRubrics();
            AddRubrics.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var AddRubricsLevel = new Forms.AddRubricsLevel();
            AddRubricsLevel.Show();
        }

        private void AddAssesment_Click(object sender, EventArgs e)
        {
            var AddAssessment = new Forms.AddAssessment();
            AddAssessment.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var AddAssessmentComponent = new Forms.AddAssessmentComponent();
            AddAssessmentComponent.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var StudentEvaluation = new Forms.StudentEvaluation();
            StudentEvaluation.Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            var Result = new Forms.Result();
            Result.Show();
        }
    }
}
