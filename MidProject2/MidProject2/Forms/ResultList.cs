﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using Microsoft.Reporting.WinForms;

namespace MidProject2.Forms
{
    public partial class ResultList : Form
    {
        SqlCommand cmd;
        SqlDataAdapter da;
        SqlDataReader dr;
        DataTable dt;
     
        

        public ResultList()
        {
            InitializeComponent();
        }

        private void ResultList_Load(object sender, EventArgs e)
        {
            var cc = Configuration.getInstance().getConnection();

            dt = new DataTable();
            //con5.Open();
            dr = null;
            cmd = new SqlCommand("SELECT  MAX(MeasurementLevel) AS MaxLvl FROM RubricLevel ", cc);
            dr = cmd.ExecuteReader();

            while (dr.Read())
            {
                maxlvltxt.Text = dr["MaxLvl"].ToString();


            }
            dr.Close();
        }

        private void Loadrptbtn_Click(object sender, EventArgs e)
        {
            //Assessment Wise
//CLO Wise




            var n = Configuration.getInstance().getConnection();
            da = new SqlDataAdapter("Select Student.Id AS Stud_Id, CONCAT(Student.FirstName,Student.LastName) AS Stud_Name , AssessmentComponent.TotalMarks AS Comp_Marks , AssessmentComponent.Name AS Component , Rubric.Details AS Rubric, RubricLevel.MeasurementLevel AS Stu_RubricLevel,RubricLevel.Details AS LevelDesc , ((CONVERT(FLOAT, RubricLevel.MeasurementLevel)/'" + maxlvltxt.Text + "')*  CONVERT(FLOAT, AssessmentComponent.TotalMarks)) AS ObtainedMarks , Clo.Name AS CLO ,Assessment.Title AS Assessment FROM Student INNER JOIN StudentResult ON Student.Id = StudentResult.StudentId  INNER JOIN AssessmentComponent ON StudentResult.AssessmentComponentId = AssessmentComponent.Id INNER JOIN Rubric ON AssessmentComponent.RubricId = Rubric.Id INNER JOIN Clo ON Clo.Id = Rubric.CloId INNER JOIN Assessment ON Assessment.Id = AssessmentComponent.AssessmentId  INNER JOIN RubricLevel ON  StudentResult.RubricMeasurementId = RubricLevel.Id", n);
            dt = new DataTable();
            da.Fill(dt);
            ReportDataSource rds = new ReportDataSource("DataSet4", dt);
           

            if (Typecbx.Text == "CLO Wise")
            {
                reportViewer1.LocalReport.ReportPath = @"C:\Users\BestBoi\Desktop\LATEST\MidProject2\MidProject2\Reports\ResultList.rdlc";

            }
            else
            {
                reportViewer1.LocalReport.ReportPath = @"C:\Users\BestBoi\Desktop\LATEST\MidProject2\MidProject2\Reports\ResultListAssessmentWise.rdlc";
            }

            reportViewer1.LocalReport.DataSources.Add(rds);
            this.reportViewer1.RefreshReport();
        }
    }
}
