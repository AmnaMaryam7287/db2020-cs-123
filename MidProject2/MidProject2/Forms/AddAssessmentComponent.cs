﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace MidProject2.Forms
{
    public partial class AddAssessmentComponent : Form
    {

        SqlCommand cmd;
        SqlDataAdapter da;
        SqlDataReader dr;
        DataTable dt;
        DataSet ds;

        public AddAssessmentComponent()
        {
            InitializeComponent();
        }

        private void AddAssessmentComponent_Load(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
           
            da = new SqlDataAdapter("SELECT * FROM Assessment", con);
            dt = new DataTable();
            da.Fill(dt);
            foreach (DataRow ROW in dt.Rows)
            {
                Assidcbx.Items.Add(ROW["Id"].ToString());
               // Searchcbx.Items.Add(ROW["Id"].ToString());

            }

            da = new SqlDataAdapter("SELECT * FROM Rubric", con);
            dt = new DataTable();
            da.Fill(dt);
            foreach (DataRow ROW in dt.Rows)
            {
                Ridcbx.Items.Add(ROW["Id"].ToString());
                // Searchcbx.Items.Add(ROW["Id"].ToString());

            }

            da = new SqlDataAdapter("SELECT * FROM AssessmentComponent", con);
            dt = new DataTable();
            da.Fill(dt);
            foreach (DataRow ROW in dt.Rows)
            {
                 Searchcbx.Items.Add(ROW["Id"].ToString());

            }

            ShowData();
        }

        public void Clear()
        {
            foreach (var item in this.Controls)
            {
                if (item.GetType().Equals(typeof(TextBox)))
                {
                    TextBox t1 = item as TextBox;
                    t1.Text = string.Empty;
                }

                if (item.GetType().Equals(typeof(ComboBox)))
                {
                    ComboBox c1 = item as ComboBox;
                    c1.Text = null;
                }

            }
        }


        public void ShowData()
        {

            var oo = Configuration.getInstance().getConnection();
            cmd = new SqlCommand("Select Assessment.Id AS Ass_Id, Assessment.Title AS Ass_Name, AssessmentComponent.Id AS Ass_Comp_Id , AssessmentComponent.Name AS Ass_Comp_Name , AssessmentComponent.TotalMarks AS Comp_Marks , AssessmentComponent.DateCreated , AssessmentComponent.DateUpdated ,  Rubric.Id AS RubricId, Rubric.Details AS RubricName   FROM Assessment INNER JOIN AssessmentComponent ON Assessment.Id = AssessmentComponent.AssessmentId INNER JOIN Rubric  ON Rubric.Id = AssessmentComponent.RubricId ", oo);
            da = new SqlDataAdapter(cmd);
            dt = new DataTable();
            da.Fill(dt);
            Fullgrid.DataSource = dt;
        }

        private void Assidcbx_SelectedIndexChanged(object sender, EventArgs e)
        {
            var cc = Configuration.getInstance().getConnection();
            dt = new DataTable();
            //con5.Open();
            dr = null;
            cmd = new SqlCommand("SELECT * FROM Assessment WHERE Id = '" + Assidcbx.Text + "'", cc);
            dr = cmd.ExecuteReader();

            while (dr.Read())
            {
                Titletxt.Text = dr["Title"].ToString();


            }
            dr.Close();
        }

        private void Ridcbx_SelectedIndexChanged(object sender, EventArgs e)
        {
            var cc = Configuration.getInstance().getConnection();
            dt = new DataTable();
            //con5.Open();
            dr = null;
            cmd = new SqlCommand("SELECT * FROM Rubric WHERE Id = '" + Ridcbx.Text + "'", cc);
            dr = cmd.ExecuteReader();

            while (dr.Read())
            {
                Rnametxt.Text = dr["Details"].ToString();


            }
            dr.Close();
        }

        private void Savebtn_Click(object sender, EventArgs e)
        {
            if ( Ridcbx.Text.Trim().Length == 0 || Assidcbx.Text.Trim().Length == 0 || Cnametxt.Text.Trim().Length == 0 || Cmarkstxt.Text.Trim().Length == 0)
            {
                MessageBox.Show("Fields Should Not Be Empty!", "User Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);

            }
            else
            {

                var con = Configuration.getInstance().getConnection();
                cmd = new SqlCommand("INSERT INTO AssessmentComponent (Name, RubricId,TotalMarks,DateCreated,DateUpdated,AssessmentId) VALUES (@Name, @RubricId,@TotalMarks,@DateCreated,@DateUpdated,@AssessmentId)", con);
                cmd.Parameters.AddWithValue("@Name", Cnametxt.Text);
                cmd.Parameters.AddWithValue("@RubricId", Ridcbx.Text);
                cmd.Parameters.AddWithValue("@TotalMarks", Cmarkstxt.Text);
                cmd.Parameters.AddWithValue("@DateCreated", DateTime.Now.ToShortDateString());
                cmd.Parameters.AddWithValue("@DateUpdated", DateTime.Now.ToShortDateString());
                cmd.Parameters.AddWithValue("@AssessmentId", Assidcbx.Text);
                cmd.ExecuteNonQuery();

                MessageBox.Show("Successfully saved", "User Information", MessageBoxButtons.OK, MessageBoxIcon.Information);

                Searchcbx.Items.Clear();

                da = new SqlDataAdapter("SELECT * FROM AssessmentComponent", con);
                dt = new DataTable();
                da.Fill(dt);
                foreach (DataRow ROW in dt.Rows)
                {
                    Searchcbx.Items.Add(ROW["Id"].ToString());

                }

                ShowData();
                Clear();
                Assidcbx.Focus();
            }
        }

        private void Searchcbx_SelectedIndexChanged(object sender, EventArgs e)
        {
            var oo = Configuration.getInstance().getConnection();
            if (Searchcbx.Text.Trim().Length == 0)
            {
                cmd = new SqlCommand("Select Assessment.Id AS Ass_Id, Assessment.Title AS Ass_Name, AssessmentComponent.Id AS Ass_Comp_Id , AssessmentComponent.Name AS Ass_Comp_Name , AssessmentComponent.TotalMarks AS Comp_Marks , AssessmentComponent.DateCreated , AssessmentComponent.DateUpdated ,  Rubric.Id AS RubricId, Rubric.Details AS RubricName FROM Assessment INNER JOIN AssessmentComponent ON Assessment.Id = AssessmentComponent.AssessmentId INNER JOIN Rubric  ON Rubric.Id = AssessmentComponent.RubricId ", oo);
           
            }
            else
            {
                cmd = new SqlCommand("Select Assessment.Id AS Ass_Id, Assessment.Title AS Ass_Name, AssessmentComponent.Id AS Ass_Comp_Id , AssessmentComponent.Name AS Ass_Comp_Name , AssessmentComponent.TotalMarks AS Comp_Marks , AssessmentComponent.DateCreated , AssessmentComponent.DateUpdated ,  Rubric.Id AS RubricId, Rubric.Details AS RubricName FROM Assessment INNER JOIN AssessmentComponent ON Assessment.Id = AssessmentComponent.AssessmentId INNER JOIN Rubric  ON Rubric.Id = AssessmentComponent.RubricId WHERE AssessmentComponent.Id ='" + Searchcbx.Text + "'  ", oo);
                
           
            }
            da = new SqlDataAdapter(cmd);
            dt = new DataTable();
            da.Fill(dt);
            Fullgrid.DataSource = dt;
            //Fullgrid.Columns[4].ReadOnly = true;
        }

        private void Searchcbx_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                Searchcbx.Text = null;
            }
        }

        private void Updatebtn_Click(object sender, EventArgs e)
        {
            if (AssCompIdtxt.Text.Trim().Length == 0 || Ridcbx.Text.Trim().Length == 0 || Assidcbx.Text.Trim().Length == 0 || Cnametxt.Text.Trim().Length == 0 || Cmarkstxt.Text.Trim().Length == 0)
            {
                MessageBox.Show("Fields Should Not Be Empty!", "User Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);

            }
            else
            {
                var nn = Configuration.getInstance().getConnection();
                string query = "UPDATE AssessmentComponent SET Name = '" + Cnametxt.Text + "' , TotalMarks = '" + Cmarkstxt.Text + "' , RubricId = '" + Ridcbx.Text + "' , AssessmentId = '" + Assidcbx.Text + "' , DateUpdated = '" + DateTime.Now.ToShortDateString() + "' WHERE Id = '" + AssCompIdtxt.Text + "'";
                cmd = new SqlCommand(query, nn);
                //SqlDataReader dr;
                try
                {
                    //con.Open();
                    dr = cmd.ExecuteReader();
                    MessageBox.Show("Record Updated successfully", "User Information", MessageBoxButtons.OK, MessageBoxIcon.Information);


                    while (dr.Read())
                    {
                    }
                    //con.Close();
                    dr.Close();
                }
                catch (Exception ec)
                {
                    MessageBox.Show(ec.Message);
                }

                ShowData();

                Clear();

            }
        }

        private void Fullgrid_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {

                //gets a collection that contains all the rows
                DataGridViewRow row = this.Fullgrid.Rows[e.RowIndex];
                Assidcbx.Text = row.Cells[0].Value.ToString();
                Titletxt.Text = row.Cells[1].Value.ToString();
                AssCompIdtxt.Text = row.Cells[2].Value.ToString();
                Cnametxt.Text = row.Cells[3].Value.ToString();
                Cmarkstxt.Text = row.Cells[4].Value.ToString();
                Ridcbx.Text = row.Cells[7].Value.ToString();
                Rnametxt.Text = row.Cells[8].Value.ToString();


            }
        }

        private void Cmarkstxt_TextChanged(object sender, EventArgs e)
        {

        }

        private void Deletebtn_Click(object sender, EventArgs e)
        {
            if (AssCompIdtxt.Text.Trim().Length == 0 || Ridcbx.Text.Trim().Length == 0 || Assidcbx.Text.Trim().Length == 0 || Cnametxt.Text.Trim().Length == 0 || Cmarkstxt.Text.Trim().Length == 0)
            {
                MessageBox.Show("Fields Should Not Be Empty!", "User Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);

            }
            else
            {

                DialogResult result = MessageBox.Show("YOU SURE WANT TO DELETE IT?", "QUESTION", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (result == DialogResult.Yes)
                {
                    var o = Configuration.getInstance().getConnection();
                    cmd = new SqlCommand("DELETE  from AssessmentComponent WHERE Id = '" + AssCompIdtxt.Text + "'", o);
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Record Deleted successfully", "User Information", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    Searchcbx.Items.Clear();
                   
                    da = new SqlDataAdapter("SELECT * FROM AssessmentComponent", o);
                    dt = new DataTable();
                    da.Fill(dt);
                    foreach (DataRow ROW in dt.Rows)
                    {
                        Searchcbx.Items.Add(ROW["Id"].ToString());

                    }

                    ShowData();

                    Clear();
                }
            }
        }
    }
}
