﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace MidProject2.Forms
{
    public partial class AddAssessment : Form
    {

        SqlCommand cmd;
        SqlDataAdapter da;
        SqlDataReader dr;
        DataTable dt;
        DataSet ds;


        public AddAssessment()
        {
            InitializeComponent();
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        public void ShowData()
        {

            var oo = Configuration.getInstance().getConnection();
            cmd = new SqlCommand("Select * from Assessment ", oo);
            da = new SqlDataAdapter(cmd);
            dt = new DataTable();
            da.Fill(dt);
            Fullgrid.DataSource = dt;
        }

        public void Clear()
        {
            foreach (var item in this.Controls)
            {
                if (item.GetType().Equals(typeof(TextBox)))
                {
                    TextBox t1 = item as TextBox;
                    t1.Text = string.Empty;
                }

                //if (item.GetType().Equals(typeof(ComboBox)))
                //{
                //    ComboBox c1 = item as ComboBox;
                //    c1.Text = null;
                //}

            }
        }

        private void AddAssessment_Load(object sender, EventArgs e)
        {
            ShowData();
        }

        private void AddAssessment_Activated(object sender, EventArgs e)
        {
            Titletxt.Focus();
        }

        private void Savebtn_Click(object sender, EventArgs e)
        {
            if (Titletxt.Text.Trim().Length == 0 || Tmarkstxt.Text.Trim().Length == 0 || Tw8txt.Text.Trim().Length == 0)
            {
                MessageBox.Show("Fields Should Not Be Empty!", "User Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);

            }
            else
            {

                var con = Configuration.getInstance().getConnection();
                cmd = new SqlCommand("INSERT INTO Assessment (Title, DateCreated,TotalMarks,TotalWeightage) VALUES (@Title, @DateCreated,@TotalMarks,@TotalWeightage)", con);
                cmd.Parameters.AddWithValue("@Title", Titletxt.Text);
                cmd.Parameters.AddWithValue("@DateCreated", DateTime.Now.ToShortDateString());
                cmd.Parameters.AddWithValue("@TotalMarks", Tmarkstxt.Text);
                cmd.Parameters.AddWithValue("@TotalWeightage", Tw8txt.Text);

                cmd.ExecuteNonQuery();

                MessageBox.Show("Successfully saved", "User Information", MessageBoxButtons.OK, MessageBoxIcon.Information);


                ShowData();
                Clear();
                Titletxt.Focus();
            }
             
        }

        private void Fullgrid_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {

              


                    //gets a collection that contains all the rows
                    DataGridViewRow row = this.Fullgrid.Rows[e.RowIndex];
                    Assidtxt.Text = row.Cells[0].Value.ToString();
                    Titletxt.Text = row.Cells[1].Value.ToString();
                    Tmarkstxt.Text = row.Cells[3].Value.ToString();
                    Tw8txt.Text = row.Cells[4].Value.ToString();
                
            }
        }

        private void Updatebtn_Click(object sender, EventArgs e)
        {
            if (Assidtxt.Text.Trim().Length == 0 || Titletxt.Text.Trim().Length == 0 || Tmarkstxt.Text.Trim().Length == 0 || Tw8txt.Text.Trim().Length == 0)
            {
                MessageBox.Show("Fields Should Not Be Empty!", "User Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);

            }
            else
            {



                var nn = Configuration.getInstance().getConnection();
                string query = "UPDATE Assessment SET Title = '" + Titletxt.Text + "' , TotalMarks = '" + Tmarkstxt.Text + "' , TotalWeightage = '" + Tw8txt.Text + "'  WHERE Id = '" + Assidtxt.Text + "'";
                cmd = new SqlCommand(query, nn);
                //SqlDataReader dr;
                try
                {
                    //con.Open();
                    dr = cmd.ExecuteReader();
                    MessageBox.Show("Record Updated successfully", "User Information", MessageBoxButtons.OK, MessageBoxIcon.Information);


                    while (dr.Read())
                    {
                    }
                    //con.Close();
                    dr.Close();
                }
                catch (Exception ec)
                {
                    MessageBox.Show(ec.Message);
                }

                ShowData();

                Clear();
            }
        }

        private void Deletebtn_Click(object sender, EventArgs e)
        {
            if (Assidtxt.Text.Trim().Length == 0 || Titletxt.Text.Trim().Length == 0 || Tmarkstxt.Text.Trim().Length == 0 || Tw8txt.Text.Trim().Length == 0)
            {
                MessageBox.Show("Fields Should Not Be Empty!", "User Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);

            }
            else
            {

                DialogResult result = MessageBox.Show("YOU SURE WANT TO DELETE IT?", "QUESTION", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (result == DialogResult.Yes)
                {
                    var o = Configuration.getInstance().getConnection();
                    cmd = new SqlCommand("DELETE  from Assessment WHERE Id = '" + Assidtxt.Text + "'", o);
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Record Deleted successfully", "User Information", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    ShowData();

                    Clear();
                }
            }
        }

        private void Searchtxt_TextChanged(object sender, EventArgs e)
        {
            var oo = Configuration.getInstance().getConnection();
            if (Searchtxt.Text.Trim().Length == 0)
            {
                cmd = new SqlCommand("Select * FROM Assessment ", oo);

            }
            else
            {
                cmd = new SqlCommand("Select * FROM Assessment WHERE Title ='" + Searchtxt.Text + "'  ", oo);

            }
            da = new SqlDataAdapter(cmd);
            dt = new DataTable();
            da.Fill(dt);
            Fullgrid.DataSource = dt;
            Fullgrid.Columns[4].ReadOnly = true;
        }

        private void Searchtxt_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                Searchtxt.Clear();
            }
        }
    }
}
