﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace MidProject2
{
    class Configuration
    {
         //String ConnectionStr = @"Data Source=DESKTOP-R4IOO7N;Initial Catalog = Lab2_Homex; Integrated Security = True";
        String ConnectionStr = @"Data Source=DESKTOP-EVJE5JH;Initial Catalog=ProjectB;Integrated Security=True";
 

        SqlConnection con;
        private static Configuration _instance;
        public static Configuration getInstance()
        {
            if (_instance == null)
                _instance = new Configuration();
            return _instance;
        }
        private Configuration()
        {
            con = new SqlConnection(ConnectionStr);
            con.Open();
        }
        public SqlConnection getConnection()
        {
            return con;
        }
    }
    
}
