﻿namespace MidProject2.Forms
{
    partial class Home
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Addclobtn = new System.Windows.Forms.Button();
            this.Addstudentbtn = new System.Windows.Forms.Button();
            this.Rubricbtn = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.Attendance = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Addclobtn
            // 
            this.Addclobtn.BackColor = System.Drawing.Color.White;
            this.Addclobtn.FlatAppearance.BorderSize = 0;
            this.Addclobtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Addclobtn.Font = new System.Drawing.Font("Century Gothic", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Addclobtn.Location = new System.Drawing.Point(12, 90);
            this.Addclobtn.Name = "Addclobtn";
            this.Addclobtn.Size = new System.Drawing.Size(211, 53);
            this.Addclobtn.TabIndex = 0;
            this.Addclobtn.Text = "CLO Registration";
            this.Addclobtn.UseVisualStyleBackColor = false;
            this.Addclobtn.Click += new System.EventHandler(this.Addclobtn_Click);
            // 
            // Addstudentbtn
            // 
            this.Addstudentbtn.BackColor = System.Drawing.Color.White;
            this.Addstudentbtn.FlatAppearance.BorderSize = 0;
            this.Addstudentbtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Addstudentbtn.Font = new System.Drawing.Font("Century Gothic", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Addstudentbtn.Location = new System.Drawing.Point(12, 31);
            this.Addstudentbtn.Name = "Addstudentbtn";
            this.Addstudentbtn.Size = new System.Drawing.Size(211, 53);
            this.Addstudentbtn.TabIndex = 1;
            this.Addstudentbtn.Text = "Student Registration";
            this.Addstudentbtn.UseVisualStyleBackColor = false;
            this.Addstudentbtn.Click += new System.EventHandler(this.Addstudentbtn_Click);
            // 
            // Rubricbtn
            // 
            this.Rubricbtn.BackColor = System.Drawing.Color.White;
            this.Rubricbtn.FlatAppearance.BorderSize = 0;
            this.Rubricbtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Rubricbtn.Font = new System.Drawing.Font("Century Gothic", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Rubricbtn.Location = new System.Drawing.Point(12, 149);
            this.Rubricbtn.Name = "Rubricbtn";
            this.Rubricbtn.Size = new System.Drawing.Size(211, 53);
            this.Rubricbtn.TabIndex = 2;
            this.Rubricbtn.Text = "Add Rubrics";
            this.Rubricbtn.UseVisualStyleBackColor = false;
            this.Rubricbtn.Click += new System.EventHandler(this.Rubricbtn_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.White;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Century Gothic", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(12, 326);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(211, 53);
            this.button1.TabIndex = 3;
            this.button1.Text = "Rubric Registration";
            this.button1.UseVisualStyleBackColor = false;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.White;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Century Gothic", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(12, 267);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(211, 53);
            this.button2.TabIndex = 4;
            this.button2.Text = "Add Rubrics Level";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // Attendance
            // 
            this.Attendance.BackColor = System.Drawing.Color.White;
            this.Attendance.FlatAppearance.BorderSize = 0;
            this.Attendance.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Attendance.Font = new System.Drawing.Font("Century Gothic", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Attendance.Location = new System.Drawing.Point(12, 208);
            this.Attendance.Name = "Attendance";
            this.Attendance.Size = new System.Drawing.Size(211, 53);
            this.Attendance.TabIndex = 5;
            this.Attendance.Text = "Attendance";
            this.Attendance.UseVisualStyleBackColor = false;
            this.Attendance.Click += new System.EventHandler(this.Attendance_Click);
            // 
            // Home
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Teal;
            this.ClientSize = new System.Drawing.Size(820, 424);
            this.Controls.Add(this.Attendance);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.Rubricbtn);
            this.Controls.Add(this.Addstudentbtn);
            this.Controls.Add(this.Addclobtn);
            this.Name = "Home";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Home";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button Addclobtn;
        private System.Windows.Forms.Button Addstudentbtn;
        private System.Windows.Forms.Button Rubricbtn;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button Attendance;
    }
}