﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
//using System.Text.RegularExpressions;

namespace MidProject2.Forms
{
    public partial class Attendance : Form
    {

        SqlCommand cmd;
        SqlDataAdapter da;
        SqlDataReader dr;
        DataTable dt;
        DataSet ds;

        public Attendance()
        {
            InitializeComponent();
        }

        private void Attendance_Load(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            //SELECT TOP 2 Name FROM Lookup ORDER BY LookupId DESC
            da = new SqlDataAdapter("SELECT * FROM Lookup WHERE Category = 'ATTENDANCE_STATUS'", con);
            dt = new DataTable();
            da.Fill(dt);
            foreach (DataRow ROW in dt.Rows)
            {
                Attendancecbx.Items.Add(ROW["Name"].ToString());

            }

            da = new SqlDataAdapter("SELECT * FROM Student ", con);
            dt = new DataTable();
            da.Fill(dt);
            foreach (DataRow ROW in dt.Rows)
            {
                Stuidcbx.Items.Add(ROW["Id"].ToString());

            }

            Check();

            dr = null;
            cmd = new SqlCommand("SELECT * FROM ClassAttendance ", con);
            dr = cmd.ExecuteReader();

            while (dr.Read())
            {
                AttIdtxt.Text = dr["Id"].ToString();
            

            }

            dr.Close();

            ShowData();

        }

        void Check()
        {
            

                int i = 0;
                var cc = Configuration.getInstance().getConnection();
                cmd = new SqlCommand("SELECT * FROM ClassAttendance WHERE AttendanceDate = '" + DateTime.Now.ToShortDateString() + "'", cc);
                da = new SqlDataAdapter(cmd);
                ds = new DataSet();
                da.Fill(ds,"ClassAttendance");
                if (ds.Tables[i].Rows.Count > 0)
                {


                    //MessageBox.Show("Yes Entry");
                 }
                 else
                {
                    cmd = new SqlCommand("INSERT INTO ClassAttendance (AttendanceDate) VALUES (@AttendanceDate)", cc);
                    cmd.Parameters.AddWithValue("@AttendanceDate", DateTime.Now.ToShortDateString());
                    cmd.ExecuteNonQuery();
                    //MessageBox.Show("No Entry");
                }   
        }


        public void Clear()
        {
            foreach (var item in this.Controls)
            {
                if (item.GetType().Equals(typeof(TextBox)))
                {
                    TextBox t1 = item as TextBox;
                    t1.Text = string.Empty;
                }

                if (item.GetType().Equals(typeof(ComboBox)))
                {
                    ComboBox c1 = item as ComboBox;
                    c1.Text = null;
                }

            }
        }


        public void ShowData()
        {

            var oo = Configuration.getInstance().getConnection();
            cmd = new SqlCommand("Select StudentAttendance.StudentId AS Id, Student.RegistrationNumber AS RegNo, CONCAT(Student.FirstName,Student.LastName) AS FullName , Lookup.Name AS Attendance ,ClassAttendance.AttendanceDate from Student LEFT JOIN StudentAttendance ON Student.Id = StudentAttendance.StudentId INNER JOIN Lookup ON Lookup.LookupId = StudentAttendance.AttendanceStatus INNER JOIN ClassAttendance ON ClassAttendance.Id = StudentAttendance.AttendanceId", oo);
            da = new SqlDataAdapter(cmd);
            dt = new DataTable();
            da.Fill(dt);
            Fullgrid.DataSource = dt;
        }


        private void Stuidcbx_SelectedIndexChanged(object sender, EventArgs e)
        {
            var cc = Configuration.getInstance().getConnection();
            dt = new DataTable();
            //con5.Open();
            dr = null;
            cmd = new SqlCommand("SELECT RegistrationNumber , CONCAT(FirstName,LastName) AS FullName FROM Student WHERE Id = '" + Stuidcbx.Text + "'", cc);
            dr = cmd.ExecuteReader();

            while (dr.Read())
            {
                FullNametxt.Text = dr["FullName"].ToString();
                StuRegtxt.Text = dr["RegistrationNumber"].ToString();


            }
            dr.Close();
        }

        private void Attendancecbx_SelectedIndexChanged(object sender, EventArgs e)
        {
            var cc = Configuration.getInstance().getConnection();
            dt = new DataTable();
            //con5.Open();
            dr = null;
            cmd = new SqlCommand("SELECT * FROM Lookup WHERE Name = '" + Attendancecbx.Text + "'", cc);
            dr = cmd.ExecuteReader();

            while (dr.Read())
            {
                LUpIdtxt.Text = dr["LookupId"].ToString();
                
            }
            dr.Close();
        }

        private void Savebtn_Click(object sender, EventArgs e)
        {
            if (Stuidcbx.Text.Trim().Length == 0 || LUpIdtxt.Text.Trim().Length == 0 || AttIdtxt.Text.Trim().Length == 0 || Attendancecbx.Text.Trim().Length == 0 )
            {
                MessageBox.Show("Fields Should Not Be Empty!", "User Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);

            }

            else
            {

                var con = Configuration.getInstance().getConnection();
                cmd = new SqlCommand("INSERT INTO StudentAttendance (AttendanceId, StudentId,AttendanceStatus) VALUES (@AttendanceId, @StudentId,@AttendanceStatus)", con);
                cmd.Parameters.AddWithValue("@AttendanceId", AttIdtxt.Text);
                cmd.Parameters.AddWithValue("@StudentId", Stuidcbx.Text);
                cmd.Parameters.AddWithValue("@AttendanceStatus", LUpIdtxt.Text);
                cmd.ExecuteNonQuery();
                MessageBox.Show("Successfully saved", "User Information", MessageBoxButtons.OK, MessageBoxIcon.Information);

                ShowData();
                Clear();
                Check();

                dateTimePicker1.Value = System.DateTime.Now;
              
                SqlDataReader r = null;
                cmd = new SqlCommand("SELECT * FROM ClassAttendance ", con);
                r = cmd.ExecuteReader();

                while (r.Read())
                {
                    AttIdtxt.Text = r["Id"].ToString();


                }
                r.Close();


                Stuidcbx.Focus();


            }
        }

        private void Attendance_Activated(object sender, EventArgs e)
        {
            Stuidcbx.Focus();

           
        }

        private void Updatebtn_Click(object sender, EventArgs e)
        {

            if (LUpIdtxt.Text.Trim().Length == 0 || Stuidcbx.Text.Trim().Length == 0)
            {
                MessageBox.Show("Fields Should Not Be Empty!", "User Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);

            }
            else
            {



                var nn = Configuration.getInstance().getConnection();
                string query = "UPDATE StudentAttendance SET StudentId = '" + Stuidcbx.Text + "' , AttendanceStatus = '" + LUpIdtxt.Text + "'  WHERE StudentId = '" + Stuidcbx.Text + "'";
                cmd = new SqlCommand(query, nn);
                //SqlDataReader dr;
                try
                {
                    //con.Open();
                    dr = cmd.ExecuteReader();
                    MessageBox.Show("Record Updated successfully", "User Information", MessageBoxButtons.OK, MessageBoxIcon.Information);


                    while (dr.Read())
                    {
                    }
                    //con.Close();
                    dr.Close();
                }
                catch (Exception ec)
                {
                    MessageBox.Show(ec.Message);
                }

                ShowData();

                Clear();
            }
            }

        private void Deletebtn_Click(object sender, EventArgs e)
        {

            if (LUpIdtxt.Text.Trim().Length == 0 || Stuidcbx.Text.Trim().Length == 0)
            {
                MessageBox.Show("Fields Should Not Be Empty!", "User Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);

            }
            else
            {

                DialogResult result = MessageBox.Show("YOU SURE WANT TO DELETE IT?", "QUESTION", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (result == DialogResult.Yes)
                {
                    var o = Configuration.getInstance().getConnection();
                    cmd = new SqlCommand("DELETE  from StudentAttendance WHERE StudentId = '" + Stuidcbx.Text + "'", o);
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Record Deleted successfully", "User Information", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    ShowData();

                    Clear();
                }
            }
        }

        private void Fullgrid_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                //gets a collection that contains all the rows
                DataGridViewRow row = this.Fullgrid.Rows[e.RowIndex];
                Stuidcbx.Text = row.Cells[0].Value.ToString();
                StuRegtxt.Text = row.Cells[1].Value.ToString();
                FullNametxt.Text = row.Cells[2].Value.ToString();
                Attendancecbx.Text = row.Cells[3].Value.ToString();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var AttendanceList = new Forms.AttendanceList();
            AttendanceList.Show();

                
            //if (Regex.IsMatch(dateTimePicker1.Text, "ClassAttendance.AttendanceDate"))
            //{

            //}
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            Searchtxt.Text = dateTimePicker1.Text +" "+ Timetxt.Text;

            var oo = Configuration.getInstance().getConnection();
            cmd = new SqlCommand("Select StudentAttendance.StudentId AS Id, Student.RegistrationNumber AS RegNo, CONCAT(Student.FirstName,Student.LastName) AS FullName , Lookup.Name AS Attendance ,ClassAttendance.AttendanceDate from Student LEFT JOIN StudentAttendance ON Student.Id = StudentAttendance.StudentId INNER JOIN Lookup ON Lookup.LookupId = StudentAttendance.AttendanceStatus INNER JOIN ClassAttendance ON ClassAttendance.Id = StudentAttendance.AttendanceId WHERE  ClassAttendance.AttendanceDate = '"+ Searchtxt.Text+"'",oo);
            da = new SqlDataAdapter(cmd);
            dt = new DataTable();
            da.Fill(dt);
            Fullgrid.DataSource = dt;
        }
        }
    }

