﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace MidProject2.Forms
{
    public partial class AddRubricsLevel : Form
    {

        SqlCommand cmd;
        SqlDataAdapter da;
        SqlDataReader dr;
        DataTable dt;
        DataSet ds;

        public AddRubricsLevel()
        {
            InitializeComponent();
        }


        public void Clear()
        {
            foreach (var item in this.Controls)
            {
                if (item.GetType().Equals(typeof(TextBox)))
                {
                    TextBox t1 = item as TextBox;
                    t1.Text = string.Empty;
                }

                if (item.GetType().Equals(typeof(ComboBox)))
                {
                    ComboBox c1 = item as ComboBox;
                    c1.Text = null;
                }

            }
        }


        public void ShowData()
        {

            var oo = Configuration.getInstance().getConnection();
            cmd = new SqlCommand("Select Rubric.Id AS RubricId, Rubric.Details AS RubricName, RubricLevel.Id AS RubricLevelId,RubricLevel.Details AS LevelDescription  from Rubric LEFT JOIN RubricLevel ON Rubric.Id = RubricLevel.RubricId ", oo);
            da = new SqlDataAdapter(cmd);
            dt = new DataTable();
            da.Fill(dt);
            Fullgrid.DataSource = dt;
        }

        private void AddRubricsLevel_Load(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            da = new SqlDataAdapter("SELECT * FROM Rubric", con);
            dt = new DataTable();
            da.Fill(dt);
            foreach (DataRow ROW in dt.Rows)
            {
                Ridcbx.Items.Add(ROW["Id"].ToString());

            }

            ShowData();
            Fullgrid.BringToFront();
        }

        private void Ridcbx_SelectedIndexChanged(object sender, EventArgs e)
        {
            var cc = Configuration.getInstance().getConnection();
            dt = new DataTable();
            //con5.Open();
            dr = null;
            cmd = new SqlCommand("SELECT * FROM Rubric WHERE Id = '" + Ridcbx.Text + "'", cc);
            dr = cmd.ExecuteReader();

            while (dr.Read())
            {
                Rnametxt.Text = dr["Details"].ToString();


            }
            dr.Close();
        }

        private void Addlvlbtn_Click(object sender, EventArgs e)
        {

            if (lvltxt.Text.Trim().Length == 0 || lvldesctxt.Text.Trim().Length == 0)
            {
                MessageBox.Show("Fields Should Not Be Empty!", "User Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);

            }
            else
            {
                int row = 0;
                Addgrid.Rows.Add();
                row = Addgrid.Rows.Count - 2;
                Addgrid["MeasurementLevel", row].Value = lvltxt.Text;
                Addgrid["Details", row].Value = lvldesctxt.Text;

                lvltxt.Clear();
                lvldesctxt.Clear();
                lvltxt.Focus();

                Addgrid.BringToFront();
            }

           

        }

        private void Savebtn_Click(object sender, EventArgs e)
        {

             if (Ridcbx.Text.Trim().Length == 0 || Rnametxt.Text.Trim().Length == 0 )
            {
                MessageBox.Show("Fields Should Not Be Empty!", "User Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);

            }

            else
            {

                for (int i = 0; i < Addgrid.Rows.Count - 1; i++)
                {

                    var con = Configuration.getInstance().getConnection();
                    cmd = new SqlCommand("INSERT INTO RubricLevel (RubricId, Details,MeasurementLevel) VALUES (@RubricId, '" + Addgrid.Rows[i].Cells[1].Value + "' , '" + Addgrid.Rows[i].Cells[0].Value + "')", con);
                    cmd.Parameters.AddWithValue("@RubricId", Ridcbx.Text);
                    cmd.ExecuteNonQuery();
                   
                    

                }

                MessageBox.Show("Successfully saved", "User Information", MessageBoxButtons.OK, MessageBoxIcon.Information);


                ShowData();
                Clear();
                Ridcbx.Focus();
                Addgrid.Rows.Clear();
//                Addgrid.DataSource = null;
                Fullgrid.BringToFront();


            }
        }

        private void AddRubricsLevel_Activated(object sender, EventArgs e)
        {
            Ridcbx.Focus();
        }
    }
}
