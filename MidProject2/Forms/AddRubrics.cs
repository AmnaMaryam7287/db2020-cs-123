﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace MidProject2.Forms
{
    public partial class AddRubrics : Form
    {

        SqlCommand cmd;
        SqlDataAdapter da;
        SqlDataReader dr;
        DataTable dt;
        DataSet ds;

        public AddRubrics()
        {
            InitializeComponent();
        }

        private void AddRubrics_Activated(object sender, EventArgs e)
        {
            Cloidcbx.Focus();
        }

        private void AddRubrics_Load(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            da = new SqlDataAdapter("SELECT * FROM Clo ", con);
            dt = new DataTable();
            da.Fill(dt);
            foreach (DataRow ROW in dt.Rows)
            {
                Cloidcbx.Items.Add(ROW["Id"].ToString());

            }

            ShowData();
        }

        private void Cloidcbx_SelectedIndexChanged(object sender, EventArgs e)
        {
            var cc = Configuration.getInstance().getConnection();
            dt = new DataTable();
            //con5.Open();
            dr = null;
            cmd = new SqlCommand("SELECT * FROM Clo WHERE Id = '" + Cloidcbx.Text + "'", cc);
            dr = cmd.ExecuteReader();

            while (dr.Read())
            {
                Clotxt.Text = dr["Name"].ToString();


            }
            dr.Close();
        }


        public void Clear()
        {
            foreach (var item in this.Controls)
            {
                if (item.GetType().Equals(typeof(TextBox)))
                {
                    TextBox t1 = item as TextBox;
                    t1.Text = string.Empty;
                }

                if (item.GetType().Equals(typeof(ComboBox)))
                {
                    ComboBox c1 = item as ComboBox;
                    c1.Text = null;
                }

            }
        }


        public void ShowData()
        {

            var oo = Configuration.getInstance().getConnection();
            cmd = new SqlCommand("Select Clo.Id AS CLOId, Clo.Name AS CLOName, Rubric.Id AS RubricId,Rubric.Details AS RubricDetail  from Clo LEFT JOIN Rubric ON Clo.Id = Rubric.CloId ", oo);
            da = new SqlDataAdapter(cmd);
            dt = new DataTable();
            da.Fill(dt);
            Fullgrid.DataSource = dt;
        }

        private void Savebtn_Click(object sender, EventArgs e)
        {

            if (Cloidcbx.Text.Trim().Length == 0 || Clotxt.Text.Trim().Length == 0 || Ridtxt.Text.Trim().Length == 0 || Rnametxt.Text.Trim().Length == 0)
            {
                MessageBox.Show("Fields Should Not Be Empty!", "User Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);

            }

            else
            {

                var con = Configuration.getInstance().getConnection();
                cmd = new SqlCommand("INSERT INTO Rubric (Id, Details,Cloid) VALUES (@Id, @Details,@Cloid)", con);
                cmd.Parameters.AddWithValue("@Id", Ridtxt.Text);
                cmd.Parameters.AddWithValue("@Details", Rnametxt.Text);
                cmd.Parameters.AddWithValue("@Cloid", Cloidcbx.Text);
                cmd.ExecuteNonQuery();
                MessageBox.Show("Successfully saved", "User Information", MessageBoxButtons.OK, MessageBoxIcon.Information);

                ShowData();
                Clear();
                Cloidcbx.Focus();
            }
        }

        private void Updatebtn_Click(object sender, EventArgs e)
        {
            if (Cloidcbx.Text.Trim().Length == 0 || Clotxt.Text.Trim().Length == 0 || Ridtxt.Text.Trim().Length == 0 || Rnametxt.Text.Trim().Length == 0)
            {
                MessageBox.Show("Fields Should Not Be Empty!", "User Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);

            }

            else
            {
                var nn = Configuration.getInstance().getConnection();
                string query = "UPDATE Rubric SET CloId = '" + Cloidcbx.Text + "' , Details = '" + Rnametxt.Text + "' , Id = '" + Ridtxt.Text + "' WHERE Id = '" + Ridtxt.Text + "' AND CloId = '" + Cloidcbx.Text + "' ";
                cmd = new SqlCommand(query, nn);
                //SqlDataReader dr;
                try
                {
                    //con.Open();
                    dr = cmd.ExecuteReader();
                    MessageBox.Show("Record Updated successfully", "User Information", MessageBoxButtons.OK, MessageBoxIcon.Information);


                    while (dr.Read())
                    {
                    }
                    //con.Close();
                    dr.Close();
                }
                catch (Exception ec)
                {
                    MessageBox.Show(ec.Message);
                }

                ShowData();

                Clear();
            }
        }

        private void Fullgrid_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                //gets a collection that contains all the rows
                DataGridViewRow row = this.Fullgrid.Rows[e.RowIndex];
                Cloidcbx.Text = row.Cells[0].Value.ToString();
                Clotxt.Text = row.Cells[1].Value.ToString();
                Ridtxt.Text = row.Cells[2].Value.ToString();
                Rnametxt.Text = row.Cells[3].Value.ToString();
            }
        }

        private void Deletebtn_Click(object sender, EventArgs e)
        {
             if (Cloidcbx.Text.Trim().Length == 0 || Clotxt.Text.Trim().Length == 0 || Ridtxt.Text.Trim().Length == 0 || Rnametxt.Text.Trim().Length == 0)
            {
                MessageBox.Show("Fields Should Not Be Empty!", "User Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);

            }

            else
             {

                 DialogResult result = MessageBox.Show("YOU SURE WANT TO DELETE IT?", "QUESTION", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                 if (result == DialogResult.Yes)
                 {
                     var o = Configuration.getInstance().getConnection();
                     cmd = new SqlCommand("DELETE  from Rubric WHERE Id = '" + Ridtxt.Text + "' AND CloId = '" + Cloidcbx.Text + "'", o);
                     cmd.ExecuteNonQuery();
                     MessageBox.Show("Record Deleted successfully", "User Information", MessageBoxButtons.OK, MessageBoxIcon.Information);

                     ShowData();

                     Clear();
                 }
             }
        }

        private void Searchtxt_TextChanged(object sender, EventArgs e)
        {
            if (Searchtxt.Text.Trim().Length == 0)
            {
                var oo = Configuration.getInstance().getConnection();
                cmd = new SqlCommand("Select Clo.Id AS CLOId, Clo.Name AS CLOName, Rubric.Id AS RubricId,Rubric.Details AS RubricDetail  from Clo LEFT JOIN Rubric ON Clo.Id = Rubric.CloId ", oo);
                da = new SqlDataAdapter(cmd);
                dt = new DataTable();
                da.Fill(dt);
                Fullgrid.DataSource = dt;
            }
            else
            {
                var oo = Configuration.getInstance().getConnection();
                cmd = new SqlCommand("Select Clo.Id AS CLOId, Clo.Name AS CLOName, Rubric.Id AS RubricId,Rubric.Details AS RubricDetail  from Clo LEFT JOIN Rubric ON Clo.Id = Rubric.CloId WHERE Clo.Id ='" + Searchtxt.Text + "'", oo);
                da = new SqlDataAdapter(cmd);
                dt = new DataTable();
                da.Fill(dt);
                Fullgrid.DataSource = dt;
            }

            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var RubricsList = new Forms.RubricsList();
            RubricsList.Show();
        }
    }
}
