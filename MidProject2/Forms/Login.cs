﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MidProject2
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            if (usernametxt.Text == "admin" && passwordtxt.Text == "admin@1")
            {
                var Home = new Forms.Home();
                this.Hide();
                Home.Show();
            }
            else
            {
                MessageBox.Show("Invalid UserName Or Password!", "Errror Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                usernametxt.Focus();
            }


            
        }

        private void usernametxt_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                passwordtxt.Focus();
            }
        }

        private void passwordtxt_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Enter)
            {
                loginbtn.Focus();
            }
        }


        private void Login_Activated(object sender, EventArgs e)
        {
            usernametxt.Focus();
        }
    }
}
